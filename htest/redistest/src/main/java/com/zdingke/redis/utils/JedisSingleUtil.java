package com.zdingke.redis.utils;

import java.io.IOException;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import redis.clients.jedis.JedisPoolConfig;

//该工具类只适合redis集群模式
public class JedisSingleUtil {

    private static final Logger log = LoggerFactory.getLogger(JedisSingleUtil.class);
    private static String REDISCONF = "redis_pool.properties";
    private static CachedClient cachedClient;

    private JedisSingleUtil() {

    }

    static {
        JedisPoolConfig config = new JedisPoolConfig();
        Properties properties = new Properties();
        try {
            properties.load(JedisClusterUtil.class.getClassLoader().getResourceAsStream(REDISCONF));
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }
        config.setMaxTotal(Integer.parseInt(properties.getProperty("redis.pool.maxTotal")));
        config.setMaxIdle(Integer.parseInt(properties.getProperty("redis.pool.maxIdle")));
        config.setMaxWaitMillis(Long.valueOf(properties.getProperty("redis.pool.maxWait")));
        String redisHost = properties.getProperty("redis.host", "");
        String redisPort = properties.getProperty("redis.port", "6379");
        String redisPassword = properties.getProperty("redis.password", "");
        cachedClient = new RedisClient(config, redisHost, Integer.parseInt(redisPort), redisPassword);
    }

    public static CachedClient getCachedClient() {
        return cachedClient;
    }

}
