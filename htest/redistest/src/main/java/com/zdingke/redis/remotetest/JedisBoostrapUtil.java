package com.zdingke.redis.remotetest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zdingke.redis.utils.CachedClient;
import com.zdingke.redis.utils.JedisSingleUtil;

public class JedisBoostrapUtil {
    private static final Logger log = LoggerFactory.getLogger(JedisBoostrapUtil.class);
    private static CachedClient cc = JedisSingleUtil.getCachedClient();
    private JedisBoostrapUtil() {

    }

    public static void main(String[] args) throws Exception {
        if (args.length == 0) {
            System.out.println("# options to choose :");
            System.out.println("-get [metricname]");
            System.out.println("-set [metricname] [metricvalue]");
            System.out.println("-delete [metricname]");
        } else if (args[0].equals("-get")) {
            log.info(cc.getCached(args[1]));
        } else if (args[0].equals("-set")) {
            cc.setCached(args[1], args[2]);
        } else if (args[0].equals("-delete")) {
            cc.del(args[1]);
        }
    }
}
