EXPLAIN(http://www.zdingke.com/2016/10/20/hive%E8%A7%A3%E6%9E%90sql%E5%BE%97%E5%88%B0%E5%85%83%E6%95%B0%E6%8D%AE/)



SQLSERVICE(http://www.zdingke.com/2016/11/22/194/)
HIVE WITH TEZ

--------------------------------------------------------------------------------<br />
tez编译：<br />
&nbsp; ● 安装maven<br />
wget http://repos.fedorapeople.org/repos/dchen/apache-maven/epel-apache-maven.repo -O /etc/yum.repos.d/epel-apache-maven.repo<br />
yum -y install apache-maven<br />
&nbsp; ● 安装protobuf-2.5.0<br />
下载：http://code.google.com/p/protobuf/downloads/list<br />
tar -xzf protobuf-2.5.0.tar.gz&nbsp;<br />
./configure --prefix=/usr/local/protobuf	make &amp;&amp; make install<br />
ln -s /usr/local/protobuf-2.5.0/bin/protoc /usr/bin/protoc<br />
&nbsp; ● 编译<br />
mvn clean package -DskipTests=true -Dmaven.javadoc.skip=true<br />
编译成功后得到tez-dist/target/tez-*.*.*.tar.gz&nbsp;<br />
--------------------------------------------------------------------------------<br />
<br />
tez配置：<br />
<br />
编辑tez-site.xml<br />
hadoop fs -copyFromLocal tez-*.*.*.tar.gz &nbsp;/user/hadoop/tez<br />
tar zxvf tez-*.*.*.tar.gz -C tez-*.*.*<br />
在$HADOOP_HOME/etc/hadoop/目录下创建tez-site.xml文件并编辑tez-site.xml文件内容<br />
&lt;configuration&gt;<br />
&nbsp; &nbsp; &nbsp; &nbsp; &lt;property&gt;<br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;name&gt;tez.lib.uris&lt;/name&gt;<br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;value&gt;${fs.defaultFS}/apps/tez-0.5.3.tar.gz&lt;/value&gt;//tez.lib.uris 的值为tez-0.5.3.tar.gz 在hdfs上的目录地址<br />
&nbsp; &nbsp; &nbsp; &nbsp; &lt;/property&gt;<br />
&lt;/configuration&gt;<br />
<br />
编辑hadoop-env.sh&nbsp;<br />
export TEZ_HOME=/oneapm/local/tez-0.5.3<br />
for jar in `ls $TEZ_HOME |grep jar`; do<br />
&nbsp; &nbsp; export HADOOP_CLASSPATH=$HADOOP_CLASSPATH:$TEZ_HOME/$jar<br />
done<br />
for jar in `ls $TEZ_HOME/lib`; do<br />
&nbsp; &nbsp; export HADOOP_CLASSPATH=$HADOOP_CLASSPATH:$TEZ_HOME/lib/$jar<br />
done<br />
<br />
编辑mapred-site.xml<br />
&nbsp;&lt;property&gt;<br />
&nbsp; &nbsp; &nbsp; &nbsp;&lt;name&gt;mapreduce.framework.name&lt;/name&gt;<br />
&nbsp; &nbsp; &nbsp; &nbsp;&lt;value&gt;yarn-tez&lt;/value&gt;<br />
&lt;/property&gt;<br />
--------------------------------------------------------------------------------<br />
<div>
	<br />
</div>