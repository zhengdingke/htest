package com.zdingke.hive.sqlservice;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.zdingke.hive.model.QueryTask;

public class SQLExec {
    private static final Log LOG = LogFactory.getLog(SQLExec.class);
    private final static String QUERYID_PREFIX = "TEZ";
    private static ConcurrentMap<String, BlockingQueue<List<QueryTask>>> taskMap = new ConcurrentHashMap<String, BlockingQueue<List<QueryTask>>>();
    private HQLSessionManager hsm;

    public SQLExec() {
        hsm = new HQLSessionManager();
    }

    public void run(String db, String cmds) {
        BlockingQueue<List<QueryTask>> queueTask = getTaskQueue(db);
        List<QueryTask> taskList = paserQueryTask(db, cmds);
        queueTask.add(taskList);
        SessionItem item = hsm.createSessionItem(db);
        item.setQueryTaskQueue(queueTask);
    }

    public List<QueryTask> paserQueryTask(String db, String cmds) {

        String[] cmdArray = cmds.split(";");
        List<QueryTask> queryList = new ArrayList<QueryTask>();
        generateQueryId(db);
        for (int i = 0; i < cmdArray.length; i++) {
            String cmd = cmdArray[i];
            String queryId = generateQueryId(db);
            QueryTask queryTask = new QueryTask(cmd, queryId, db);
            queryList.add(queryTask);
        }
        return queryList;
    }

    private String generateQueryId(String userid) {
        GregorianCalendar gc = new GregorianCalendar();
        return userid + "_" + String.format("%1$4d%2$02d%3$02d%4$02d%5$02d%6$02d", gc.get(Calendar.YEAR), gc.get(Calendar.MONTH) + 1,
                gc.get(Calendar.DAY_OF_MONTH), gc.get(Calendar.HOUR_OF_DAY), gc.get(Calendar.MINUTE), gc.get(Calendar.SECOND)) + "_"
                + UUID.randomUUID().toString();
    }

    public BlockingQueue<List<QueryTask>> getTaskQueue(String user) {
        return taskMap.computeIfAbsent(user, k -> new LinkedBlockingQueue<>());
    }

    public ConcurrentMap<String, BlockingQueue<List<QueryTask>>> getTaskMap() {
        return taskMap;
    }
}
