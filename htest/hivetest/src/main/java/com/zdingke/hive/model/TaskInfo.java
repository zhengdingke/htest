package com.zdingke.hive.model;

public class TaskInfo {

    private String processName;
    private String vertexState;
    private int succeededTaskCount;
    private int totalTaskCount;
    private int runningTaskCount;
    private int failedTaskAttemptCount;
    private int killedTaskCount;
    private int pendingTaskCount;
    private static final String SPACE = " ";

    public TaskInfo() {
    }

    public TaskInfo(String processName, String vertexState, int succeededTaskCount, int totalTaskCount, int runningTaskCount, int failedTaskAttemptCount,
            int killedTaskCount) {
        this.processName = processName;
        this.vertexState = vertexState;
        this.succeededTaskCount = succeededTaskCount;
        this.totalTaskCount = totalTaskCount;
        this.runningTaskCount = runningTaskCount;
        this.failedTaskAttemptCount = failedTaskAttemptCount;
        this.killedTaskCount = killedTaskCount;
    }

    public String getProcessName() {
        return processName;
    }

    public void setProcessName(String processName) {
        this.processName = processName;
    }

    public String getVertexState() {
        return vertexState;
    }

    public void setVertexState(String vertexState) {
        this.vertexState = vertexState;
    }

    public int getSucceededTaskCount() {
        return succeededTaskCount;
    }

    public void setSucceededTaskCount(int succeededTaskCount) {
        this.succeededTaskCount = succeededTaskCount;
    }

    public int getTotalTaskCount() {
        return totalTaskCount;
    }

    public void setTotalTaskCount(int totalTaskCount) {
        this.totalTaskCount = totalTaskCount;
    }

    public int getRunningTaskCount() {
        return runningTaskCount;
    }

    public void setRunningTaskCount(int runningTaskCount) {
        this.runningTaskCount = runningTaskCount;
    }

    public int getFailedTaskAttemptCount() {
        return failedTaskAttemptCount;
    }

    public void setFailedTaskAttemptCount(int failedTaskAttemptCount) {
        this.failedTaskAttemptCount = failedTaskAttemptCount;
    }

    public int getKilledTaskCount() {
        return killedTaskCount;
    }

    public void setKilledTaskCount(int killedTaskCount) {
        this.killedTaskCount = killedTaskCount;
    }

    public int getPendingTaskCount() {
        return pendingTaskCount;
    }

    public String toRest() {
        double process = (Math.abs(succeededTaskCount) * 1.0f / Math.abs(totalTaskCount) * 1.0f) * 100;
        return processName + SPACE + Double.valueOf(Math.floor(process)).longValue() + "%";
    }

}
