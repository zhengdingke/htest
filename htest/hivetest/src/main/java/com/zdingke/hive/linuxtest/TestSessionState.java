package com.zdingke.hive.linuxtest;

import java.io.IOException;

import org.apache.hadoop.hive.cli.CliSessionState;
import org.apache.hadoop.hive.ql.session.SessionState;
import org.apache.hive.hcatalog.common.HCatUtil;

import com.zdingke.common.context.HadoopContext;

public class TestSessionState {

    public static void main(String[] args) throws IOException {
        SessionState sessionState = new CliSessionState(HCatUtil.getHiveConf(HadoopContext.getConf()));
        sessionState.setCurrentDatabase("default");
        SessionState.start(sessionState);
    }
}
