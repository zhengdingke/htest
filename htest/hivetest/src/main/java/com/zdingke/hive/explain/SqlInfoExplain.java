package com.zdingke.hive.explain;

import java.io.IOException;
import java.util.List;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hive.cli.CliSessionState;
import org.apache.hadoop.hive.conf.HiveConf;
import org.apache.hadoop.hive.ql.Context;
import org.apache.hadoop.hive.ql.metadata.HiveException;
import org.apache.hadoop.hive.ql.parse.ASTNode;
import org.apache.hadoop.hive.ql.parse.BaseSemanticAnalyzer;
import org.apache.hadoop.hive.ql.parse.ParseDriver;
import org.apache.hadoop.hive.ql.parse.ParseException;
import org.apache.hadoop.hive.ql.parse.ParseUtils;
import org.apache.hadoop.hive.ql.parse.SemanticAnalyzerFactory;
import org.apache.hadoop.hive.ql.session.SessionState;

import com.google.common.collect.Lists;
import com.zdingke.common.context.HadoopContext;
import com.zdingke.hive.model.DbOperEnum;
import com.zdingke.hive.model.SqlExplainInfo;
import com.zdingke.hive.model.TableOperEnum;

public class SqlInfoExplain {

    private static final String USEDB = "TOK_SWITCHDATABASE";
    private SqlExplainInfo explainInfo = new SqlExplainInfo();
    private HiveConf conf = HadoopContext.getHiveConf();
    private ASTNode subTree;
    private SqlTableExplain sqlTableExplain;
    private String currentDb;

    public SqlInfoExplain(String sql) throws HiveException, IOException, ParseException {
        validate(sql);
        this.explainInfo.setSql(sql);
        for (String subSql : sql.split(";")) {
            if ("".equals(subSql)) {
                continue;
            }
            this.sqlTableExplain = new SqlTableExplain(subSql);
            this.subTree = sqlTableExplain.getASTNodeBySql();
            init();
        }

    }

    private void semanticAnalyze(String sql, HiveConf conf) throws IOException, ParseException, HiveException {
        Context ctx = new Context(conf);
        ctx.setCmd(sql);
        ctx.setHDFSCleanup(true);
        ctx.setResFile(new Path("EXPLAIN"));
        ASTNode tree = null;
        for (String subSql : sql.split(";")) {
            tree = syntaxParser(subSql);
            BaseSemanticAnalyzer sem = SemanticAnalyzerFactory.get(conf, tree);
            SessionState.get().initTxnMgr(conf);
            sem.analyze(tree, ctx);
            sem.validate();
        }
    }

    private ASTNode syntaxParser(String sql) throws ParseException {
        ParseDriver pd = new ParseDriver();
        ASTNode tree = pd.parse(sql);
        tree = ParseUtils.findRootNonNullToken(tree);
        tree = (ASTNode) tree.getChild(0);
        return tree;
    }

    private void validate(String sql) throws HiveException, IOException, ParseException {
        startSessionState();
        semanticAnalyze("explain " + sql, conf);
    }

    private void startSessionState() {
        SessionState ss = new CliSessionState(conf);
        ss.startForExplain(ss);
    }

    private void init() throws HiveException {
        setOperationLevel();
    }

    private void setOperationLevel() throws HiveException {
        for (DbOperEnum dbOper : DbOperEnum.values()) {
            if (dbOper.toString().equals(subTree.getText())) {
                if (subTree.getText().equals(USEDB)) {
                    currentDb = subTree.getChild(0).getText();
                }
                setDbOperationMap(dbOper);
                return;
            }
        }
        setTableOperationMap();
    }

    private void setDbOperationMap(DbOperEnum dbOper) {
        String dbName = subTree.getChild(0).getText();
        List<String> operList = this.explainInfo.getDbMapsOperationList().getOrDefault(dbName, Lists.newArrayList());
        if (!isContain(dbOper.getValue(), operList)) {
            operList.add(dbOper.getValue());
        }
        this.explainInfo.getDbMapsOperationList().put(dbName, operList);
    }

    private boolean isContain(String oper, List<String> operList) {
        return operList.contains(oper);
    }

    private void setTableOperationMap() throws HiveException {
        List<ASTNode> nodeList = Lists.newArrayList();
        sqlTableExplain.getTableNameRecursively(subTree, nodeList);
        String dbName = null;
        String tableName = null;
        for (ASTNode node : nodeList) {
            if (node.getChild(1) == null) {
                dbName = currentDb;
                tableName = node.getChild(0).getText();
            } else {
                dbName = node.getChild(0).getText();
                tableName = node.getChild(1).getText();
            }
            List<String> operList = this.explainInfo.getTableMapsOperationList().getOrDefault(dbName + "." + tableName, Lists.newArrayList());
            String oper = getFirstOper(node);
            if (!isContain(oper, operList)) {
                operList.add(oper);
            }
            this.explainInfo.getTableMapsOperationList().put(dbName + "." + tableName, operList);
        }
    }


    private String getFirstOper(ASTNode tree) throws HiveException {
        ASTNode operTree = tree;
        while (operTree.getParent() != null) {
            for (TableOperEnum oper : TableOperEnum.values()) {
                if (oper.toString().equals(operTree.getParent().getText())) {
                    return oper.getValue();
                }
            }
            operTree = (ASTNode) operTree.getParent();
        }
        throw new HiveException("can not find the Oper!!");
    }


    public SqlExplainInfo getExplainInfo() {
        return explainInfo;
    }

    public void setExplainInfo(SqlExplainInfo explainInfo) {
        this.explainInfo = explainInfo;
    }
}
