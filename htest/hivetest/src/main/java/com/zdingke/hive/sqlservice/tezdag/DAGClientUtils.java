package com.zdingke.hive.sqlservice.tezdag;

import java.text.NumberFormat;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.yarn.api.records.ApplicationId;
import org.apache.hadoop.yarn.util.ConverterUtils;
import org.apache.tez.dag.api.TezConfiguration;
import org.apache.tez.dag.api.client.DAGClient;
import org.apache.tez.dag.api.client.DAGClientImpl;

import com.zdingke.common.context.HadoopContext;

public class DAGClientUtils {
    private static final Log LOG = LogFactory.getLog(DAGClientUtils.class);
    private static final char SEPARATOR = '_';
    private static final String DAG = "dag";

    private DAGClientUtils() {

    }

    public static DAGClient createDagClient(String yarnId, String dagSeq) {
        LOG.info("create dag client yarnId:" + yarnId + "| dagNum:" + dagSeq);
        TezConfiguration tezConf = new TezConfiguration(HadoopContext.getHiveConf());
        ApplicationId id = ConverterUtils.toApplicationId(yarnId);
        String dagId = createTezDAGID(id, dagSeq);
        return new DAGClientImpl(id, dagId, tezConf, null);
    }

    private static String createTezDAGID(ApplicationId id, String dagSeq) {
        NumberFormat fmt = NumberFormat.getInstance();
        fmt.setGroupingUsed(false);
        fmt.setMinimumIntegerDigits(4);
        return (new StringBuilder(DAG)).append(SEPARATOR).append(id.getClusterTimestamp()).append(SEPARATOR).append(fmt.format(id.getId())).append(SEPARATOR)
                .append(dagSeq).toString();
    }
}
