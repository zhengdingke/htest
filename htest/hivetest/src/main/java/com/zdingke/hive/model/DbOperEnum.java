package com.zdingke.hive.model;

public enum DbOperEnum {
    TOK_DROPDATABASE("drop"), TOK_CREATEDATABASE("create"), TOK_SHOWDATABASES("show"), TOK_SWITCHDATABASE("use");

    private final String value;

    DbOperEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
