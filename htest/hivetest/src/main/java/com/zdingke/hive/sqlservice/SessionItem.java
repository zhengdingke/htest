package com.zdingke.hive.sqlservice;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hive.cli.CliSessionState;
import org.apache.hadoop.hive.conf.HiveConf;
import org.apache.hadoop.hive.metastore.api.Schema;
import org.apache.hadoop.hive.ql.CommandNeedRetryException;
import org.apache.hadoop.hive.ql.Driver;
import org.apache.hadoop.hive.ql.processors.CommandProcessor;
import org.apache.hadoop.hive.ql.processors.CommandProcessorFactory;
import org.apache.hadoop.hive.ql.processors.CommandProcessorResponse;
import org.apache.hadoop.hive.ql.session.SessionState;
import org.apache.hive.hcatalog.common.HCatUtil;

import com.zdingke.hive.model.QueryTask;

public class SessionItem implements Runnable {

    private static final Log LOG = LogFactory.getLog(SessionItem.class);
    private SessionState sessionState;
    Driver driver;
    HiveConf hiveConf;
    BlockingQueue<List<QueryTask>> queryTaskQueue = new LinkedBlockingQueue<List<QueryTask>>();
    Thread runnable;
    String user;
    Boolean isInit = false;

    public SessionItem(Configuration conf, String user) {
        try {
            this.hiveConf = HCatUtil.getHiveConf(conf);
        } catch (IOException e) {
            LOG.error(e);
        }
        this.user = user;
    }

    public void setThread(Thread runnable) {
        this.runnable = runnable;
    }

    public BlockingQueue<List<QueryTask>> getQueryTaskQueue() {
        return queryTaskQueue;
    }

    public void setQueryTaskQueue(BlockingQueue<List<QueryTask>> queryTaskQueue) {
        this.queryTaskQueue = queryTaskQueue;
    }

    public boolean isAlive() {
        return runnable.isAlive();
    }

    public String getSessionId() {
        if (sessionState == null) {
            return null;
        }
        return sessionState.getSessionId();
    }

    public SessionState getSessionState() {
        return sessionState;
    }

    protected void init() {
        sessionState = new CliSessionState(hiveConf);
        sessionState.setCurrentDatabase(user);
        SessionState.start(sessionState);
        isInit = true;
    }


    @Override
    public void run() {
        while (true) {
            if (!isInit) {
                init();
            }
            List<QueryTask> queryTasks = null;
            try {
                LOG.info("begin get task!!");
                queryTasks = queryTaskQueue.take();
                LOG.info("get task from queue!! |queryTasks size:" + queryTasks.size() + "|cmd:" + queryTasks.get(0).getCmd());
            } catch (InterruptedException e) {
                LOG.error(e.getMessage(), e);
            }
            if (queryTasks == null) {
                continue;
            }
            LOG.info("YARN-APP-ID:" + SessionState.get().getTezSession().getSession().getAppMasterApplicationId());
            runQueryTasksHandleUtil(queryTasks);
        }
    }


    private void runQueryTasksHandleUtil(List<QueryTask> queryTasks) {
        int res = 0;
        for (int i = 0; i < queryTasks.size() && res == 0; i++) {
            QueryTask queryTask = queryTasks.get(i);
            String cmd = queryTask.getCmd();
            String queryId = queryTask.getQueryId();
            LOG.info("start running queryId:" + queryId + "== cmd:" + cmd);
            String cmdTrimmed = cmd.trim();
            String[] tokens = cmdTrimmed.split("\\s+");
            String cmdFirst = cmdTrimmed.substring(tokens[0].length()).trim();
            res = hqlExec(tokens[0], cmdFirst, queryTask);
        }
    }

    private int hqlExec(String cmd, String cmdFirst, QueryTask queryTask) {
        CommandProcessor proc = null;
        try {
            proc = CommandProcessorFactory.get(cmd);
        } catch (SQLException e) {
            LOG.error(e.getMessage(), e);
        }
        if (proc == null) {
            LOG.error("SQL error!CommandProcessor is null! sql: " + queryTask.getCmd());
            statusReport(queryTask, "SQL  error!");
            return -1;
        }
        if (!(proc instanceof Driver)) {
            return noDriverHandler(cmdFirst, queryTask, proc);
        } else {
            driver = (Driver) proc;
        }
        int res = driver.compile(queryTask.getCmd());
        if (res != 0) {
            statusReport(queryTask, "SQL Compile FAILED!" + "|" + driver.getErrorMsg());
            return -1;
        }
        setQueryId(hiveConf, queryTask.getQueryId());
        return driverHandler(queryTask);
    }

    private int driverHandler(QueryTask queryTask) {
        try {
            LOG.info("driver handler!!");
            CommandProcessorResponse response = driver.run();
            driverGetResult(queryTask);
            if (response.getResponseCode() != 0) {
                statusReport(queryTask, "SQL execute Failed!" + "|" + response.getErrorMessage());
                return -1;
            }
        } catch (IOException | CommandNeedRetryException e) {
            LOG.error(e.getMessage(), e);
            statusReport(queryTask, "SQL execute exception!" + "|" + e.getMessage());
            return -1;
        }
        return 0;
    }

    private int noDriverHandler(String cmdFirst, QueryTask queryTask, CommandProcessor proc) {
        int response;
        try {
            LOG.info("no driver handler!!");
            CommandProcessorResponse process = proc.run(cmdFirst);
            response = process.getResponseCode();
            if (response != 0) {
                statusReport(queryTask, "SQL Running Failed!" + process.getErrorMessage());
                return -1;
            } else {
                commandGetResult(queryTask, process);
                return 0;
            }
        } catch (CommandNeedRetryException | IOException e) {
            LOG.error(e.getMessage(), e);
            statusReport(queryTask, "SQL Running Failed!" + "|" + e.getMessage());
        }
        return 0;
    }

    private void commandGetResult(QueryTask queryTask, CommandProcessorResponse process) throws IOException, CommandNeedRetryException {
        if (queryTask.isConfCmd()) {
            return;
        }
    }

    private void driverGetResult(QueryTask queryTask) throws IOException, CommandNeedRetryException {
        if (queryTask.isConfCmd()) {
            return;
        }
        List<String> res = new ArrayList<String>();
        List<String> result = new ArrayList<String>();
        Schema schema = driver.getSchema();
        try {
            while (driver.getResults(res)) {
                result.addAll(res.subList(0, res.size()));
            }
        } catch (IOException e) {
            LOG.error(e.getMessage(), e);
            throw e;
        }

        LOG.info("end running sucess|queryId:" + queryTask.getQueryId() + "|result size:" + result.size() + "|schema:" + schema.getFieldSchemasSize());
    }

    private void setQueryId(HiveConf conf, String queryId) {
        conf.setVar(HiveConf.ConfVars.HIVEQUERYID, queryId);
    }

    private void statusReport(QueryTask queryTask, String message) {
        if (queryTask.isConfCmd()) {
            return;
        }
    }

}
