package com.zdingke.hive.linuxtest;

import java.io.IOException;
import java.util.stream.Collectors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.hive.ql.metadata.HiveException;
import org.apache.hadoop.hive.ql.parse.ParseException;

import com.zdingke.hive.explain.SqlInfoExplain;

public class TestSqlExplain {

    private static final Log LOG = LogFactory.getLog(TestSqlExplain.class);

    public static void main(String[] args) throws ParseException, HiveException, IOException {

        if (args.length != 1) {
            LOG.info("parm include <sql>");
        } else {
            SqlInfoExplain se = new SqlInfoExplain(args[0]);
            se.getExplainInfo().getDbMapsOperationList().entrySet().forEach(entry->{
                LOG.info("db:"+entry.getKey());
                LOG.info("oper:" + entry.getValue().stream().collect(Collectors.joining(",")));
            });
            se.getExplainInfo().getTableMapsOperationList().entrySet().forEach(entry -> {
                LOG.info("table:" + entry.getKey());
                LOG.info("oper:" + entry.getValue().stream().collect(Collectors.joining(",")));
            });
        }
    }
}
