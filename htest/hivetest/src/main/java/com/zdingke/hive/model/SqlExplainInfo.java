package com.zdingke.hive.model;

import java.util.List;
import java.util.Map;

import com.google.common.collect.Maps;

public class SqlExplainInfo {
    private String sql;
    private Map<String, List<String>> tableMapsOperationList = Maps.newHashMap(); // <tableName,select|insert|update|delete>
    private Map<String, List<String>> dbMapsOperationList = Maps.newHashMap();

    public String getSql() {
        return sql;
    }

    public void setSql(String sql) {
        this.sql = sql;
    }

    public Map<String, List<String>> getTableMapsOperationList() {
        return tableMapsOperationList;
    }

    public void setTableMapsOperationList(Map<String, List<String>> tableMapsOperationList) {
        this.tableMapsOperationList = tableMapsOperationList;
    }

    public Map<String, List<String>> getDbMapsOperationList() {
        return dbMapsOperationList;
    }

    public void setDbMapsOperationList(Map<String, List<String>> dbMapsOperationList) {
        this.dbMapsOperationList = dbMapsOperationList;
    }



}
