package com.zdingke.hive.model;

import java.util.List;


public class TaskStatistic {
    private List<TaskInfo> taskInfos;
    private int sumSucceedTaskCount;
    private int allTaskCount;

    public TaskStatistic(List<TaskInfo> taskInfos) {
        this.taskInfos = taskInfos;
        init();
    }

    private void init() {
        for (TaskInfo info : taskInfos) {
            sumSucceedTaskCount += info.getSucceededTaskCount();
            allTaskCount += Math.abs(info.getTotalTaskCount());
        }
    }

    public int getSumSucceedTaskCount() {
        return sumSucceedTaskCount;
    }

    public int getAllTaskCount() {
        return allTaskCount;
    }
}
