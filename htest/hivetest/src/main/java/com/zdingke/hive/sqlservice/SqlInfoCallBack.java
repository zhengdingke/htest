package com.zdingke.hive.sqlservice;

import java.util.concurrent.ExecutionException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.zdingke.hive.model.JobProcessLog;
import com.zdingke.hive.model.TaskStatistic;

public class SqlInfoCallBack {

    private static final Log LOG = LogFactory.getLog(SqlInfoCallBack.class);
    private SqlInfoCallBack() {
    }

    public static void pushStageLog(JobProcessLog jobProcessLog) throws ExecutionException {
        TaskStatistic ts = new TaskStatistic(jobProcessLog.getTaskInfos());
        LOG.info("queryId:" + jobProcessLog.getQueryId() + "====taskInfo=====" + "SUCCESSCOUNT:" + ts.getSumSucceedTaskCount()
                + "|ALLTASKCOUNT:" + ts.getAllTaskCount());
    }

    public static void pushYarnId(String queryId, String yarnAppId) {
        LOG.info("pushyarn! " + "|queryId:" + queryId + "|yarnAppId:" + yarnAppId);

    }
}
