package com.zdingke.hive.explain;

import java.util.List;

import org.antlr.runtime.tree.CommonTree;
import org.apache.hadoop.hive.ql.lib.Node;
import org.apache.hadoop.hive.ql.parse.ASTNode;
import org.apache.hadoop.hive.ql.parse.HiveParser;
import org.apache.hadoop.hive.ql.parse.ParseDriver;
import org.apache.hadoop.hive.ql.parse.ParseException;
import org.apache.hadoop.hive.ql.parse.ParseUtils;

import com.google.common.collect.Lists;

public class SqlTableExplain {

    private String sql;

    public SqlTableExplain(String sql) {
        this.sql = sql;
    }

    public void getTableNameRecursively(ASTNode node, List<ASTNode> nodeList) {
        if (node != null && node.getChildren() != null) {
            for (Node childNode : node.getChildren()) {
                int type = ((CommonTree) childNode).getType();
                if (type == HiveParser.TOK_ALLCOLREF) {// 设置过滤条件去除a.*（a不是表）
                    break;
                }
                if (type == HiveParser.TOK_TABTYPE || type == HiveParser.TOK_TABNAME) {
                    nodeList.add((ASTNode) childNode);
                }
                getTableNameRecursively((ASTNode) childNode, nodeList);
            }
        }
    }

    public List<String> getAllTableName() throws ParseException {
        List<String> tableNameList = Lists.newArrayList();
        List<ASTNode> nodeList = Lists.newArrayList();
        getTableNameRecursively(getASTNodeBySql(), nodeList);
        for (ASTNode node : nodeList) {
            if (node.getChild(1) == null) {
                tableNameList.add(node.getChild(0).getText());
            } else {
                tableNameList.add(node.getChild(0).getText() + "." + node.getChild(1).getText());
            }
        }
        return tableNameList;
    }

    public ASTNode getASTNodeBySql() throws ParseException {
        ParseDriver pd = new ParseDriver();
        ASTNode tree = pd.parse(sql);
        tree = ParseUtils.findRootNonNullToken(tree);
        System.out.println(tree.dump());
        return tree;
    }
}
