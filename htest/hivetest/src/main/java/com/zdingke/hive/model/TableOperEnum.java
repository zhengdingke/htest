package com.zdingke.hive.model;

public enum TableOperEnum {
    TOK_INSERT("insert"), TOK_QUERY("select"), TOK_SELECT("select"), TOK_DROPTABLE("drop"), TOK_TRUNCATETABLE("truncate"), TOK_LOAD("load"), TOK_CREATETABLE(
            "create"), TOK_ALTERTABLE("alter"), TOK_DESCTABLE("desc"), TOK_SHOWTABLES("show");
    private final String value;

    TableOperEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
