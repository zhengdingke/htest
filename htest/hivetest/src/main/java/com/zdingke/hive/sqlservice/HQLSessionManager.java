package com.zdingke.hive.sqlservice;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.zdingke.common.context.HadoopContext;
import com.zdingke.hive.common.HiveConstants;

public class HQLSessionManager {

    private static final Log LOG = LogFactory.getLog(HQLSessionManager.class);
    private static ConcurrentMap<String, Set<SessionItem>> userSessionMap = new ConcurrentHashMap<>();

    public HQLSessionManager() {
    }

    public synchronized SessionItem createSessionItem(String user) {
        Set<SessionItem> items = userSessionMap.computeIfAbsent(user, k -> new HashSet<SessionItem>());
        if (!items.isEmpty()) {// 是否有合适的sessionStem进行重用
            int maxSize = HadoopContext.getHiveConf().getInt(HiveConstants.ODP_HQLSESSION_PERUSER_SIZE, HiveConstants.ODP_DEFAULT_HQLSESSION_PERUSER_SIZE);
            if (items.size() >= maxSize) {
                return items.iterator().next();
            } else {
                int taskSize = items.iterator().next().getQueryTaskQueue().size();
                if (taskSize <= 1) {
                    return items.iterator().next();
                }
            }
        }
        LOG.info("NEW ITEM!!");
        SessionItem item = newItem(user);
        items.add(item);
        userSessionMap.put(user, items);
        return item;
    }

    private synchronized SessionItem newItem(String user) {
        SessionItem item1 = new SessionItem(HadoopContext.getConf(), user);
        Thread itemThread = new Thread(item1);
        itemThread.start();
        item1.setThread(itemThread);
        return item1;
    }

    public Set<SessionItem> getSessionItem(String user) {
        return userSessionMap.get(user);
    }

}
