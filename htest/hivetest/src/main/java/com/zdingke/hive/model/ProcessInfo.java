package com.zdingke.hive.model;

public class ProcessInfo {

    private String verticesSummary;
    private String progressStr;
    private String elapsedTime;

    public ProcessInfo(String verticesSummary, String progressStr, String elapsedTime) {
        this.verticesSummary = verticesSummary;
        this.progressStr = progressStr;
        this.elapsedTime = elapsedTime;
    }

    public String getVerticesSummary() {
        return verticesSummary;
    }

    public void setVerticesSummary(String verticesSummary) {
        this.verticesSummary = verticesSummary;
    }

    public String getProgressStr() {
        return progressStr;
    }

    public void setProgressStr(String progressStr) {
        this.progressStr = progressStr;
    }

    public String getElapsedTime() {
        return elapsedTime;
    }

    public void setElapsedTime(String elapsedTime) {
        this.elapsedTime = elapsedTime;
    }

    @Override
    public String toString() {
        return "ProcessInfo [verticesSummary=" + verticesSummary + ", progressStr=" + progressStr + ", elapsedTime=" + elapsedTime + "]";
    }

}
