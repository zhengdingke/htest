package com.zdingke.hive.linuxtest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.zdingke.hive.explain.SqlTableExplain;

public class TestSqlTableExplain {
    private static final Log LOG = LogFactory.getLog(TestSqlTableExplain.class);

    public static void main(String[] args) {
        if (args.length != 1) {
            LOG.info("parm include <sql>");
        } else {
            new SqlTableExplain(args[0]);
        }
    }
}
