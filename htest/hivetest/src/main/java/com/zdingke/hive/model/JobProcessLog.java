package com.zdingke.hive.model;

import java.util.List;

import com.alibaba.fastjson.JSON;

public class JobProcessLog {

    private String queryId;
    private List<TaskInfo> taskInfos;
    private ProcessInfo processInfo;
    private static final String SPACE = " ";

    public JobProcessLog(String queryId, List<TaskInfo> taskInfos, ProcessInfo processInfo) {
        this.queryId = queryId;
        this.taskInfos = taskInfos;
        this.processInfo = processInfo;
    }

    public String getQueryId() {
        return queryId;
    }

    public void setQueryId(String queryId) {
        this.queryId = queryId;
    }

    public List<TaskInfo> getTaskInfos() {
        return taskInfos;
    }

    public void setTaskInfos(List<TaskInfo> taskInfos) {
        this.taskInfos = taskInfos;
    }

    public ProcessInfo getProcessInfo() {
        return processInfo;
    }

    public void setProcessInfo(ProcessInfo processInfo) {
        this.processInfo = processInfo;
    }

    @Override
    public String toString() {
        return JSON.toJSONString(this);
    }

    public String toRest() {
        String begin = this.getProcessInfo().getElapsedTime() + SPACE;
        StringBuilder sb = new StringBuilder();
        sb.append(begin);
        for (TaskInfo taskInfo : this.getTaskInfos()) {
            sb.append(taskInfo.toRest()).append(SPACE);
        }
        return sb.toString();
    }

}
