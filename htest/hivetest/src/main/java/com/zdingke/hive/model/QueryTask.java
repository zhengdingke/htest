package com.zdingke.hive.model;

import java.util.List;

public class QueryTask {

    private String user;
    private String cmd;
    private String queryId;
    private boolean confCmd = false;
    private List<String> confCmdList;

    public QueryTask() {

    }

    public QueryTask(String cmd, String queryId, String user) {
        this.cmd = cmd;
        this.queryId = queryId;
        this.user = user;
    }

    public String getCmd() {
        return cmd;
    }

    public void setCmd(String cmd) {
        this.cmd = cmd;
    }

    public String getQueryId() {
        return queryId;
    }

    public void setQueryId(String queryId) {
        this.queryId = queryId;
    }

    public boolean isConfCmd() {
        return confCmd;
    }

    public void setConfCmd() {

    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public List<String> getConfCmdList() {
        return confCmdList;
    }

    public void setConfCmdList(List<String> confCmdList) {
        this.confCmdList = confCmdList;
    }
}
