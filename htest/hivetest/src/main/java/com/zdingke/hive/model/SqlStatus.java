package com.zdingke.hive.model;

import java.util.List;

public class SqlStatus {

    private Status status;
    List<QueryTask> queryTasks;
    private String sqlId;

    public String getSqlId() {
        return sqlId;
    }

    public void setSqlId(String sqlId) {
        this.sqlId = sqlId;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public List<QueryTask> getQueryTasks() {
        return queryTasks;
    }

    public void setQueryTasks(List<QueryTask> queryTasks) {
        this.queryTasks = queryTasks;
    }

    @Override
    public String toString() {
        return "SqlStatus [status=" + status + ", queryTasks=" + queryTasks + ", sqlId=" + sqlId + "]";
    }

    public enum Status {
        PENDING, SUCCESS, FAILED, RUNNING, KILLED, UNKNOWN
    }
}
