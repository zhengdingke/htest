package com.zdingke.hive.linuxtest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zdingke.hive.sqlservice.SQLExec;

public class TestSqlExec {
    private static final Logger log = LoggerFactory.getLogger(TestSqlExec.class);

    public static void main(String[] args) throws IOException {
        SQLExec exec = new SQLExec();
        while (true) {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            String sql = br.readLine();
            if (!StringUtils.isBlank(sql)) {
                exec.run("default", sql);
            }
        }
    }
}
