/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.apache.hadoop.hive.ql.exec.tez;

import static org.apache.tez.dag.api.client.DAGStatus.State.RUNNING;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.ExecutionException;

import jline.TerminalFactory;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.hive.conf.HiveConf;
import org.apache.hadoop.hive.ql.exec.Heartbeater;
import org.apache.hadoop.hive.ql.lockmgr.HiveTxnManager;
import org.apache.hadoop.hive.ql.session.SessionState;
import org.apache.tez.dag.api.DAG;
import org.apache.tez.dag.api.TezException;
import org.apache.tez.dag.api.client.DAGClient;
import org.apache.tez.dag.api.client.DAGStatus;
import org.apache.tez.dag.api.client.Progress;
import org.apache.tez.dag.api.client.StatusGetOpts;
import org.apache.tez.dag.api.client.VertexStatus;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.zdingke.hive.model.JobProcessLog;
import com.zdingke.hive.model.ProcessInfo;
import com.zdingke.hive.model.TaskInfo;
import com.zdingke.hive.sqlservice.SqlInfoCallBack;

/**
 * TezJobMonitor keeps track of a tez job while it's being executed. It will
 * print status to the console and retrieve final status of the job after
 * completion.
 */
public class TezJobMonitor {
    private static final Log LOG = LogFactory.getLog(TezJobMonitor.class);
    private static final int COLUMN_1_WIDTH = 16;

    // private final PerfLogger perfLogger = PerfLogger.getPerfLogger();
    private static final int CHECKINTERVAL = 200;
    private static final int MAXRETRYINTERVAL = 2500;
    private Set<String> completed;

    /* Pretty print the values */
    private final NumberFormat secondsFormat;
    private static final List<DAGClient> shutdownList;

    private StringBuilder diagnostics;

    private String lastFlag;
    static {
        shutdownList = Collections.synchronizedList(new LinkedList<DAGClient>());
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                for (int i = 0; i < shutdownList.size(); i++) {
                    TezJobMonitor.killRunningJobs();
                }
                try {
                    for (TezSessionState s : TezSessionPoolManager.getInstance().getOpenSessions()) {
                        TezSessionPoolManager.getInstance().close(s, false);
                    }
                } catch (Exception e) {
                    LOG.error(e);
                }
            }
        });
    }

    public TezJobMonitor() {
        secondsFormat = new DecimalFormat("#0.00");
    }

    public static void initShutdownHook() {
        Preconditions.checkNotNull(shutdownList, "Shutdown hook was not properly initialized");
    }

    /**
     * NOTE: Use this method only if isUnixTerminal is true. Gets the width of
     * the terminal
     *
     * @return - width of terminal
     */
    public int getTerminalWidth() {
        return TerminalFactory.get().getWidth();
    }

    /**
     * monitorExecution handles status printing, failures during execution and
     * final status retrieval.
     *
     * @param dagClient
     *            client that was used to kick off the job
     * @param txnMgr
     *            transaction manager for this operation
     * @param conf
     *            configuration file for this operation
     * @return int 0 - success, 1 - killed, 2 - failed
     */
    public int monitorExecution(final DAGClient dagClient, HiveTxnManager txnMgr, HiveConf conf, DAG dag) throws InterruptedException {
        DAGStatus status = null;
        completed = new HashSet<String>();
        diagnostics = new StringBuilder();
        boolean running = false;
        boolean done = false;
        int failedCounter = 0;
        int rc = 0;
        DAGStatus.State lastState = null;
        Set<StatusGetOpts> opts = new HashSet<StatusGetOpts>();
        Heartbeater heartbeater = new Heartbeater(txnMgr, conf);
        long startTime = 0;
        shutdownList.add(dagClient);
        SqlInfoCallBack.pushYarnId(SessionState.get().getConf().get(SessionState.get().getQueryId()), SessionState.get().getTezSession().getSession()
                .getAppMasterApplicationId().toString());
        while (true) {

            try {
                status = dagClient.getDAGStatus(opts);
                Map<String, Progress> progressMap = status.getVertexProgress();
                DAGStatus.State state = status.getState();
                heartbeater.heartbeat();

                if (state != lastState || state == RUNNING) {
                    lastState = state;

                    switch (state) {
                    case SUBMITTED:
                        break;
                    case INITING:
                        startTime = System.currentTimeMillis();
                        break;
                    case RUNNING:
                        if (!running) {
                            startTime = System.currentTimeMillis();
                            running = true;
                        }
                        printStatusInPlace(progressMap, startTime, false, dagClient, conf);
                        break;
                    case SUCCEEDED:
                        printStatusInPlace(progressMap, startTime, false, dagClient, conf);

                        running = false;
                        done = true;
                        break;
                    case KILLED:
                        printStatusInPlace(progressMap, startTime, true, dagClient, conf);
                        running = false;
                        done = true;
                        rc = 1;
                        break;
                    case FAILED:
                    case ERROR:
                        printStatusInPlace(progressMap, startTime, true, dagClient, conf);
                        running = false;
                        done = true;
                        rc = 2;
                        break;
                    }
                }
                if (!done) {
                    Thread.sleep(CHECKINTERVAL);
                }
            } catch (Exception e) {
                if (++failedCounter % MAXRETRYINTERVAL / CHECKINTERVAL == 0 || e instanceof InterruptedException) {
                    try {
                        dagClient.tryKillDAG();
                    } catch (IOException io) {
                        LOG.error(io);
                    } catch (TezException te) {
                        LOG.error(te);
                    }
                    LOG.error(e);
                    rc = 1;
                    done = true;
                }
            } finally {
                if (done) {
                    if (rc != 0 && status != null) {
                        for (String diag : status.getDiagnostics()) {
                            diagnostics.append(diag);
                        }
                    }
                    shutdownList.remove(dagClient);
                    break;
                }
            }
        }
        return rc;
    }

    /**
     * killRunningJobs tries to terminate execution of all currently running tez
     * queries. No guarantees, best effort only.
     */
    public static void killRunningJobs() {
        for (DAGClient c : shutdownList) {
            try {
                LOG.error("Trying to shutdown DAG");
                c.tryKillDAG();
            } catch (Exception e) {
                LOG.error(e);
            }
        }
    }

    private void printStatusInPlace(Map<String, Progress> progressMap, long startTime, boolean vextexStatusFromAM, DAGClient dagClient, HiveConf conf)
            throws ExecutionException {
        int sumComplete = 0;
        int sumTotal = 0;

        SortedSet<String> keys = new TreeSet<String>(progressMap.keySet());
        List<TaskInfo> taskInfos = Lists.newArrayList();
        for (String s : keys) {
            Progress progress = progressMap.get(s);
            final int complete = progress.getSucceededTaskCount();
            final int total = progress.getTotalTaskCount();
            final int running = progress.getRunningTaskCount();
            final int failed = progress.getFailedTaskAttemptCount();
            final int killed = progress.getKilledTaskCount();

            // To get vertex status we can use DAGClient.getVertexStatus(), but
            // it will be expensive to
            // get status from AM for every refresh of the UI. Lets infer the
            // state from task counts.
            // Only if DAG is FAILED or KILLED the vertex status is fetched from
            // AM.
            VertexStatus.State vertexState = VertexStatus.State.INITIALIZING;

            // INITED state
            if (total > 0) {
                vertexState = VertexStatus.State.INITED;
                sumComplete += complete;
                sumTotal += total;
            }

            // RUNNING state
            if (complete < total && (complete > 0 || running > 0 || failed > 0)) {
                vertexState = VertexStatus.State.RUNNING;
            }

            // SUCCEEDED state
            if (complete == total) {
                vertexState = VertexStatus.State.SUCCEEDED;
                if (!completed.contains(s)) {
                    completed.add(s);

                    /*
                     * We may have missed the start of the vertex due to the 3
                     * seconds interval
                     */
                }
            }

            // DAG might have been killed, lets try to get vertex state from AM
            // before dying
            // KILLED or FAILED state
            if (vextexStatusFromAM) {
                VertexStatus vertexStatus = null;
                try {
                    vertexStatus = dagClient.getVertexStatus(s, null);
                } catch (IOException e) {
                    LOG.error(e);
                } catch (TezException e) {
                    LOG.error(e);
                }
                if (vertexStatus != null) {
                    vertexState = vertexStatus.getState();
                }
            }

            // Map 1 .......... SUCCEEDED 7 7 0 0 0 0
            String nameWithProgress = getNameWithProgress(s, complete, total);
            taskInfos.add(new TaskInfo(nameWithProgress, vertexState.toString(), complete, total, running, failed, killed));
        }

        final float progress = (sumTotal == 0) ? 0.0f : (float) sumComplete / (float) sumTotal;

        JobProcessLog jobProcessLog = createJobProcessLog(startTime, keys, taskInfos, progress);
        pushProcessLog(jobProcessLog);
    }

    private void pushProcessLog(JobProcessLog jobProcessLog) throws ExecutionException {
        String currentFlag = generateFlag(jobProcessLog.getTaskInfos());
        if (lastFlag == null || !currentFlag.equals(lastFlag)) {
            SqlInfoCallBack.pushStageLog(jobProcessLog);
        }
        lastFlag = currentFlag;
    }

    private String generateFlag(List<TaskInfo> taskInfos) {
        StringBuilder sb = new StringBuilder();
        for (TaskInfo task : taskInfos) {
            sb.append(task.getSucceededTaskCount()).append(":");
        }
        return sb.toString().substring(0, sb.length() - 1);
    }

    private JobProcessLog createJobProcessLog(long startTime, SortedSet<String> keys, List<TaskInfo> taskInfos, final float progress) {
        String verticesSummary = String.format("VERTICES: %02d/%02d", completed.size(), keys.size());
        final int progressPercent = (int) (progress * 100);
        String progressStr = "" + progressPercent + "%";
        float et = (float) (System.currentTimeMillis() - startTime) / (float) 1000;
        String elapsedTime = "ELAPSED TIME: " + secondsFormat.format(et) + " s";
        ProcessInfo processInfo = new ProcessInfo(verticesSummary, progressStr, elapsedTime);
        return new JobProcessLog(SessionState.get().getQueryId(), taskInfos, processInfo);
    }

    // Map 1 ..........
    private String getNameWithProgress(String s, int complete, int total) {
        String result = "";
        if (s != null) {
            String trimmedVName = s;

            // if the vertex name is longer than column 1 width, trim it down
            // "Tez Merge File Work" will become "Tez Merge File.."
            if (s.length() > COLUMN_1_WIDTH) {
                trimmedVName = s.substring(0, COLUMN_1_WIDTH - 1);
                trimmedVName = trimmedVName + "..";
            }

            result = trimmedVName + " ";
        }
        return result;
    }

    public String getDiagnostics() {
        return diagnostics.toString();
    }

}
