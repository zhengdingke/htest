# 下载zabbix
rpm -ivh https://mirrors.aliyun.com/zabbix/zabbix/4.4/rhel/7/x86_64/zabbix-release-4.4-1.el7.noarch.rpm

cat /etc/yum.repos.d/zabbix.repo

# 替换成为阿里镜像
sed -i 's/http:\/\/repo.zabbix.com/https:\/\/mirrors.aliyun.com\/zabbix/g' /etc/yum.repos.d/zabbix.repo

# hadoop102
yum install zabbix-server-mysql zabbix-web-mysql zabbix-agent
# hadoop103
yum install zabbix-agent
# hadoop104
yum install zabbix-agent


vim /etc/zabbix/zabbix_server.conf

vim /etc/zabbix/zabbix_agentd.conf

vim /etc/httpd/conf.d/zabbix.conf

# 启动zabbix
# hadoop102
systemctl start zabbix-server zabbix-agent httpd 
systemctl enable zabbix-server zabbix-agent httpd 
# hadoop103 \ hadoop104
systemctl start zabbix-agent

# 停止
systemctl stop zabbix-server zabbix-agent httpd 
systemctl disable zabbix-server zabbix-agent httpd 

