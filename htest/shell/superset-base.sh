yum install -y python-setuptools
yum install -y gcc gcc-c++ libffi-devel python-devel python-pip python-wheel openssl-devel cyrus-sasl-devel openldap-devel
pip install --upgrade setuptools pip -i https://pypi.douban.com/simple/
pip install apache-superset -i https://pypi.douban.com/simple/
superset db upgrade
export FLASK_APP=superset 
flask fab create-admin
superset init
pip install gunicorn -i https://pypi.douban.com/simple/


gunicorn --workers 5 --timeout 120 --bind hadoop102:8787 "superset.app:create_app()" --daemon
