package com.zdingke.kafka.newconsumer;

import java.io.IOException;
import java.util.Arrays;
import java.util.Properties;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;

public class NewConsumerApiForTopic {

    private static final String PROPERTY = "/conf/newconsumer.properties";

    public static void main(String[] args) throws IOException {
        Properties props = new Properties();
        props.load(NewConsumerApiForTopic.class.getResourceAsStream(PROPERTY));

        KafkaConsumer<String, String> consumer = new KafkaConsumer<>(props);
        consumer.subscribe(Arrays.asList(args[0]));
        while (true) {
            System.out.println("start consumer");
            ConsumerRecords<String, String> records = consumer.poll(1000);
            for (ConsumerRecord<String, String> record : records) {
                System.out.println("offset = " + record.offset() + "key = " + record.key() + "value = " + record.value());
            }
        }
    }
}
