package com.zdingke.kafka.newconsumer;

import java.io.IOException;
import java.util.Arrays;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.TopicPartition;

import com.zdingke.common.utils.Base64Util;
import com.zdingke.kafka.model.LogEntry;
import com.zdingke.kafka.utils.XStreamUtil;

public class NewConsumerApiForPartition {

    private static final Log LOG = LogFactory.getLog(NewConsumerApiForPartition.class);

    private static final String PROPERTY = "/conf/newconsumer.properties";

    public static void main(String[] args) throws IOException {
        Properties props = new Properties();
        props.load(NewConsumerApiForPartition.class.getResourceAsStream(PROPERTY));

        KafkaConsumer<String, String> consumer = new KafkaConsumer<>(props);
        String topic = args[0];
        int patitionNum = Integer.parseInt(args[1]);
        TopicPartition partition4 = new TopicPartition(topic, patitionNum);
        consumer.assign(Arrays.asList(partition4));
        consumer.seek(partition4, Long.parseLong(args[2]));
        ConsumerRecords<String, String> records = consumer.poll(1000);

        int num =0;
        for (ConsumerRecord<String, String> record : records) {
            if (++num > 1) {
                break;
            }
            LOG.info("offset =   " + record.offset() + "key = " + record.key() + "patition = " + record.partition());
            LOG.info("full data:  " + record.value());
            LogEntry log = XStreamUtil.transfer(record);
            LOG.info("message:  " + Base64Util.decode(log.getMessage()));
        }
    }
}
