package com.zdingke.kafka.utils;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.TopicPartition;

import com.google.common.collect.Lists;
import com.zdingke.common.utils.Base64Util;
import com.zdingke.kafka.model.LogEntry;
import com.zdingke.kafka.newconsumer.NewConsumerApiForPartition;

public class ListKafkaDataUtil {

    private static final Log LOG = LogFactory.getLog(NewConsumerApiForPartition.class);
    private static final String PROPERTY = "/conf/newconsumer.properties";

    public static List<String> list(String topic, String partitionNum, String offset, String listNum) throws IOException {
        List<String> result = Lists.newArrayList();
        Properties props = new Properties();
        props.load(NewConsumerApiForPartition.class.getResourceAsStream(PROPERTY));

        int patitionNum = Integer.parseInt(partitionNum);
        int dataNum = Integer.parseInt(listNum);
        KafkaConsumer<String, String> consumer = new KafkaConsumer<>(props);
        TopicPartition partition4 = new TopicPartition(topic, patitionNum);
        consumer.assign(Arrays.asList(partition4));
        consumer.seek(partition4, Long.parseLong(offset));
        ConsumerRecords<String, String> records = consumer.poll(1000);

        int num = 0;
        for (ConsumerRecord<String, String> record : records) {
            if (++num > dataNum) {
                break;
            }
            LOG.info("offset =   " + record.offset() + "key = " + record.key() + "patition = " + record.partition());
            LOG.info("full data:  " + record.value());
            LogEntry log = XStreamUtil.transfer(record);
            LOG.info("message:  " + Base64Util.decode(log.getMessage()));
            result.add(Base64Util.decode(log.getMessage()));
        }
        return result;
    }
    public static void main(String[] args) throws IOException {
        if (args.length != 4) {
            LOG.info("parm include <topic partitionNum offset datanum>");
        } else {
            list(args[0], args[1], args[2], args[3]);
        }
    }
}
