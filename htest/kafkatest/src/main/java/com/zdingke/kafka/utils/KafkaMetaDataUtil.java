package com.zdingke.kafka.utils;

import java.util.List;

import kafka.javaapi.TopicMetadata;
import kafka.javaapi.TopicMetadataRequest;
import kafka.javaapi.TopicMetadataResponse;
import kafka.javaapi.consumer.SimpleConsumer;

import com.google.common.collect.Lists;


public class KafkaMetaDataUtil {

    public static int getPartitionNum(String host, int port) {
        List<String> topics = Lists.newArrayList();
        TopicMetadataRequest req = new TopicMetadataRequest(topics);
        SimpleConsumer consumer = new SimpleConsumer(host, port, 60000, 1024 * 1024, "getPartitionNum");
        TopicMetadataResponse res = consumer.send(req);
        List<TopicMetadata> metaData = res.topicsMetadata();
        return metaData.get(0).partitionsMetadata().size();
    }

    public static void main(String[] args) {
        System.out.println(KafkaMetaDataUtil.getPartitionNum(args[0], Integer.parseInt(args[1])));
    }
}
