package com.zdingke.kafka.newconsumer;

import java.io.IOException;
import java.util.Properties;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;

public class KafkaProduce {

    private static final String PROPERTY = "/conf/producer.properties";

    public static void main(String[] args) throws IOException {

        Properties props = new Properties();
        props.load(KafkaProduce.class.getResourceAsStream(PROPERTY));

        Producer<String, String> producer = new KafkaProducer<String, String>(props);
        for (int i = 0; i < 2; i++) {
            String str = "<dingke>" + "郑丁科</dingke>";
            ProducerRecord<String, String> record = new ProducerRecord<String, String>("logs", i, i + "", com.zdingke.common.utils.Base64Util.getBASE64(str));
            producer.send(record);
        }
        producer.close();
    }
}
