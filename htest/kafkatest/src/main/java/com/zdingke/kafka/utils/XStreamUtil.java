package com.zdingke.kafka.utils;

import org.apache.kafka.clients.consumer.ConsumerRecord;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import com.zdingke.kafka.model.LogEntry;

public class XStreamUtil {

    public static LogEntry transfer(ConsumerRecord<String, String> record) {
        XStream xStream = new XStream(new DomDriver("utf-8"));
        xStream.processAnnotations(LogEntry.class);
        return (LogEntry) xStream.fromXML(record.value());
    }
}
