package com.kingdeehit.bigdata.kafka.local;

import java.util.List;
import java.util.concurrent.Callable;

import kafka.consumer.ConsumerIterator;
import kafka.consumer.KafkaStream;
import kafka.javaapi.consumer.ConsumerConnector;
import kafka.message.MessageAndMetadata;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.google.common.collect.Lists;


public class KafkaConsumerThread implements Callable {

    private static final Log LOG = LogFactory.getLog(KafkaConsumerThread.class);
    private KafkaStream<byte[], byte[]> stream;
    private final ConsumerConnector consumer;
    private List<MessageAndMetadata<byte[], byte[]>> dataList = Lists.newArrayList();

    public KafkaConsumerThread(ConsumerConnector consumer, KafkaStream<byte[], byte[]> stream, String topic) {
        this.consumer = consumer;
        this.stream = stream;
    }

    @Override
    public Object call() throws Exception {
        ConsumerIterator<byte[], byte[]> it = stream.iterator();
        while (it.hasNext()) {
            dataList.add(it.next());
            if (dataList.size() != 10000) {
                continue;
            }
            for (MessageAndMetadata<byte[], byte[]> data : dataList) {
                int partition = data.partition();
                long offset = data.offset();
                String message = new String(data.message());
                LOG.info("partition:" + partition + "|offset:" + offset + "|data:" + message);
            }
            dataList.clear();
        }
        return null;
    }
}
