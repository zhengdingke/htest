package com.zdingke.kafka.oldconsumer;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import kafka.consumer.Consumer;
import kafka.consumer.ConsumerConfig;
import kafka.consumer.KafkaStream;
import kafka.javaapi.consumer.ConsumerConnector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.google.common.collect.Maps;
import com.zdingke.common.utils.ExecutorServiceWithClientTrace;
import com.zdingke.kafka.utils.KafkaMetaDataUtil;

public class MultiThreadConsumer {

    private static Log LOG = LogFactory.getLog(MultiThreadConsumer.class);
    private static final String PROPERTY = "/conf/oldconsumer.properties";
    private final int numThreads;
    private final ConsumerConnector consumer;
    private String topic;
    private ExecutorServiceWithClientTrace executor;
    private static final int CONSUMER_TIME = 10000000;

    public MultiThreadConsumer(String topic, String host, int port) throws IOException {
        this.numThreads = KafkaMetaDataUtil.getPartitionNum(host, port);
        this.executor = new ExecutorServiceWithClientTrace(Executors.newFixedThreadPool(this.numThreads));
        this.consumer = Consumer.createJavaConsumerConnector(createConsumerConfig());
        this.topic = topic;
    }

    private static ConsumerConfig createConsumerConfig() throws IOException {
        Properties props = new Properties();
        props.load(MultiThreadConsumer.class.getResourceAsStream(PROPERTY));
        return new ConsumerConfig(props);
    }

    private void run() {
        Map<String, Integer> topicCountMap = Maps.newHashMap();
        topicCountMap.put(topic, numThreads);
        Map<String, List<KafkaStream<byte[], byte[]>>> streamMap = consumer.createMessageStreams(topicCountMap);
        List<KafkaStream<byte[], byte[]>> streams = streamMap.get(topic);
        streams.stream().forEach(s -> {
            executor.submit(new KafkaConsumerThread(consumer, s));
        });
    }

    private void shutdown() {
        if (consumer != null) {
            consumer.shutdown();
        }
        if (executor != null) {
            executor.shutdown();
        }
        try {
            if (!executor.awaitTermination(5000, TimeUnit.MILLISECONDS)) {
                LOG.error("Timed out waiting for consumer threads to shut down, exiting uncleanly");
            }
        } catch (InterruptedException e) {
            LOG.error("Timed out waiting for consumer threads to shut down, exiting uncleanly");
        }
    }

    public static void main(String[] args) throws IOException {

        if (args.length != 2) {
            LOG.info("parm include <topic host:port>");
        } else {
            LOG.info("start time!:" + new Date().toLocaleString());
            String[] str = args[1].split(":");
            MultiThreadConsumer consumer = new MultiThreadConsumer(args[0], str[0], Integer.parseInt(str[1]));
            consumer.run();
            try {
                Thread.sleep(CONSUMER_TIME);
            } catch (InterruptedException e) {
                consumer.shutdown();
            }
            consumer.shutdown();
        }
    }
}
