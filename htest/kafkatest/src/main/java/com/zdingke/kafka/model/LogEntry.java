package com.zdingke.kafka.model;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("log")
public class LogEntry {
	
	private String host;
	private String timestamp;
	private String level;
	private String thread;
	private String message;
	private String tenantId;
	private String categoryName;
	
	public String getHost() {
		return host;
	}
	public String getTimestamp() {
		return timestamp;
	}
	public String getLevel() {
		return level;
	}
	public String getThread() {
		return thread;
	}
	public String getMessage() {
		return message;
	}
	public void setHost(String host) {
		this.host = host;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	public void setLevel(String level) {
		this.level = level;
	}
	public void setThread(String thread) {
		this.thread = thread;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getTenantId() {
		return tenantId;
	}
	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

}
