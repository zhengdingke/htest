package com.zdingke.sqoop;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.util.GenericOptionsParser;
import org.apache.sqoop.Sqoop;
import org.apache.sqoop.tool.ExportTool;

import com.shsnc.control.util.toolUtil.NewHdfsConfig;

public class TestSqoopB {
    public static void main(String[] args) {
//        String url = "'jdbc:oracle:thin:@(DESCRIPTION =(ADDRESS = (PROTOCOL = TCP)(HOST = 192.168.35.176)(PORT = 1521))(LOAD_BALANCE = yes)(failover = on)(CONNECT_DATA =(SERVER = DEDICATED)(SERVICE_NAME = bighead)(FAILOVER_MODE =(TYPE = SELECT)(METHOD = BASIC)(RETRIES = 180)(DELAY = 5))))'";
        String url = "jdbc:oracle:thin:@192.168.35.176:1521:bighead";
        //xinju/Xinju_20170809
//        String username = "MVQ";
        String username = "xinju";
//        String password = "Mvq_20150504";
        String password ="Xinju_20170809";
//        String table = "TEST_XJ_COLLECTMONITOR";
        String table = "TEST1";
        String separator = ",";
        ArrayList<String> list = null;
        String[] exarg = null;
        Configuration conf = null;
        String hdfsPath = args[0];
        
        if (conf == null) {
            NewHdfsConfig dconf = new NewHdfsConfig();
            conf = dconf.getLocalConfig();
        }
        
        ExportTool exporter = new ExportTool();
        Sqoop sqoop = new Sqoop(exporter, conf);
        String[] toolArgs = null;
        

        list = new ArrayList<String>();
//      list.add("export");
        list.add("--connect");
        list.add(url); // 对应服务器的数据库的链接
        list.add("--username");
        list.add(username); // 数据库的用户名
        list.add("--password");
        list.add(password); // 数据库的密码
        list.add("--table");
        list.add(table);    
        
        list.add("--fields-terminated-by");
        list.add(separator); // 数据分隔符号
        list.add("--export-dir");
        list.add(hdfsPath);
        list.add("--outdir");
        list.add("outdir"); //生成的java文件存放路径，目录为相对目录，进行统一管理
        list.add("--input-null-string");
        list.add("//N"); //如果没有这个选项，那么在字符串类型列中，字符串"null"会被转换成空字符串
        list.add("--input-null-non-string");
        list.add("//N"); //如果没有这个选项，那么在非字符串类型的列中，空串和"null"都会被看作是null. 
        list.add("--batch");//批量
        
        System.out.println("-----------begin print list----------------");
        for(String tmp:list){
            System.out.print(tmp+" ");
        }
        System.out.println("-----------end print list-----------");

        exarg = new String[list.size()];
        exarg = list.toArray(new String[0]);
        exarg = stashChildPrgmArgs(exarg);
        GenericOptionsParser parser = null;
        try {
            parser = new GenericOptionsParser(conf, exarg);
        } catch (IOException e) {
            System.out.println("sqoop GenericOptionsParser报错："+e.getMessage());
        }
        toolArgs = parser.getRemainingArgs();
        sqoop.setConf(conf);
        list.clear();
        sqoop.run(toolArgs);
    }
    
    private static String[] stashChildPrgmArgs(String [] argv) {
        for (int i = 0; i < argv.length; i++) {
          if ("--".equals(argv[i])) {
            return Arrays.copyOfRange(argv, 0, i);
          }
        }
        return argv;
      }
}
