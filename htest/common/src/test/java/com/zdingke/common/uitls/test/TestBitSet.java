package com.zdingke.common.uitls.test;

import java.util.BitSet;

import org.junit.Test;

public class TestBitSet {

    @Test
    public void testSet() {
        BitSet bit = new BitSet();
        bit.set(100);
        bit.stream().forEach(b -> {
            System.out.println(b);
        });
    }

    @Test
    public void testSize() {
        BitSet bit = new BitSet();
        bit.set(100);
        bit.set(101);
        System.out.println(bit.size());
    }
}
