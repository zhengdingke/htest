package com.zdingke.common.uitls.test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Random;

import org.junit.Test;

import com.zdingke.common.utils.BloomFilter;
import com.zdingke.common.utils.StringUtil;

public class TestBloomFilter {

    @Test
    public void testAdd() {
        BloomFilter filter = new BloomFilter();
        while (true) {
            char c = 'a';
            c = (char) (c + (int) (Math.random() * 26));
            int num = (new Random()).nextInt(10) + 1;
            if (!filter.contains(num + "" + c + c)) {
                filter.add(num + "" + c + c);
            }
            System.out.println(filter.getCardinality());
        }
    }

    @Test
    public void testContains() {
        BloomFilter filter = new BloomFilter();
        filter.add("dingke");
        filter.add("dd1");
        System.out.println(filter.contains("dingke"));
    }

    @Test
    public void testCardinality() {
        BloomFilter filter = new BloomFilter();
        filter.add("dingke");
        filter.add("dd1");
        System.out.println(filter.getCardinality());
    }

    @Test
    public void testMd5() {
        System.out.println(StringUtil.toMD5("dingke"));
    }

    @Test
    public void testConfilct() throws IOException {
        BloomFilter filter = new BloomFilter();
        FileReader fr = new FileReader(new File("target/test-classes/healthy1.txt"));
        BufferedReader br = new BufferedReader(fr);
        String line = "";
        int num = 0;
        new StringBuilder();
        while ((line = br.readLine()) != null) {
            if (!filter.contains(line)) {
                num++;
                filter.add(line);
            }
        }
        br.close();
        fr.close();
        System.out.println("被使用到的位：" + filter.getCardinality());
        System.out.println("bitset总长：" + filter.getBitSize());
        System.out.println("占用内存：" + filter.getBitSize() / 8l);
        System.out.println("冲突率：" + (1 - ((float) num / 2410975)) * 100);
    }

    @Test
    public void testGuavaBloomFilter() {
    }

}
