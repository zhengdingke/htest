package com.zdingke.hdfs.linuxtest;

import java.io.IOException;
import java.net.URI;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

public class TestHdfs {
    public static void main(String[] args) throws IllegalArgumentException, IOException {
        String hdfspath = args[0];
        HdfsConfig dconf = new HdfsConfig();
        Configuration conf = dconf.getLocalConfig();
        FileSystem fs = null;
        FileStatus[] hadfiles = null;

        fs = FileSystem.get(URI.create(new Path(hdfspath).toString()), conf);
        hadfiles = fs.listStatus(new Path(hdfspath));
        for(FileStatus file : hadfiles){
            System.out.println(file.getPath()+"-"+file.getLen());
        }
    }
}
