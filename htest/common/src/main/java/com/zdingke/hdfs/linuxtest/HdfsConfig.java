package com.zdingke.hdfs.linuxtest;

import org.apache.hadoop.conf.Configuration;

/**
 * @author hxt
 * 生成读取本地集群的Configuration
 */
public class HdfsConfig {
	/**
	 * 日志对象
	 */
	public HdfsConfig()
	{
		
	}
	
	public Configuration getLocalConfig()
	{
		Configuration conf = new Configuration();
//		conf.addResource("/usr/hdp/2.3.0.0-2557/hadoop/conf/core-site.xml");
//		conf.addResource(new Path());n
		conf.addResource("core-site.xml");
		conf.addResource("hdfs-site.xml");
		conf.addResource("mapred-site.xml");
//		conf.addResource("/home/appxjmvq/sqoop/conf/sqoop-site.xml");
//		conf.addResource("localCluster/core-site.xml");
//		conf.addResource("localCluster/hdfs-site.xml");
//		conf.addResource("localCluster/mapred-site.xml");
		conf.set("hdp.version","2.3.0.0-2557");//传递${hdp.version}参数
		conf.set("fs.hdfs.impl",org.apache.hadoop.hdfs.DistributedFileSystem.class.getName());
		conf.set("fs.file.impl",org.apache.hadoop.fs.LocalFileSystem.class.getName());			
		conf.set("mapreduce.job.queuename","xjmvq");//配置yarn的queue队列
		/*conf.set("sqoop.metastore.client.enable.autoconnect","true");
		conf.set("sqoop.metastore.client.autoconnect.url","jdbc:hsqldb:file:/data/xjmvq/tmp/sqoop-meta/meta.db;shutdown=true");		
		conf.set("sqoop.metastore.server.location","/data/xjmvq/tmp");//本地存储路径，默认在tmp下，改为其他路径
		conf.set("hadoop.tmp.dir","/data/xjmvq/tmp");
		conf.set("mapred.tmp.dir","/data/xjmvq/tmp");*/
//		System.setProperty("java.io.tmpdir", "/data/xjmvq/tmp");
//		System.setProperty("test.build.data", "/data/xjmvq/tmp/");
//		log.info("sqoop.metastore.server.location---->"+conf.get("sqoop.metastore.server.location"));
		return conf;
	}
	

}
