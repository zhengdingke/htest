package com.zdingke.common.utils;

import java.io.IOException;
import java.util.Map;
import java.util.Set;

import javax.management.Attribute;
import javax.management.AttributeList;
import javax.management.MBeanAttributeInfo;
import javax.management.MBeanServerConnection;
import javax.management.ObjectInstance;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.google.common.base.Throwables;
import com.google.common.collect.Maps;

public class JMXPollUtil {
    private static final Log LOG = LogFactory.getLog(JMXPollUtil.class);

    public static Map<String, Map<String, String>> getAllMBeans(String host, String port, String flumeflag) throws IOException {
        MBeanServerConnection mbeanServer = createMBeanServerConn(host, port);
        Map<String, Map<String, String>> mbeanMap = Maps.newHashMap();
        Set<ObjectInstance> queryMBeans = null;
        try {
            queryMBeans = mbeanServer.queryMBeans(null, null);
        } catch (Exception ex) {
            LOG.error("Could not get Mbeans for monitoring", ex);
            Throwables.propagate(ex);
        }
        for (ObjectInstance obj : queryMBeans) {
            try {
                if (!obj.getObjectName().toString().startsWith(flumeflag)) {
                    continue;
                }
                MBeanAttributeInfo[] attrs = mbeanServer.getMBeanInfo(obj.getObjectName()).getAttributes();
                String strAtts[] = new String[attrs.length];
                for (int i = 0; i < strAtts.length; i++) {
                    strAtts[i] = attrs[i].getName();
                }
                AttributeList attrList = mbeanServer.getAttributes(obj.getObjectName(), strAtts);
                String component = obj.getObjectName().toString().substring(obj.getObjectName().toString().indexOf('=') + 1);
                Map<String, String> attrMap = Maps.newHashMap();

                for (Object attr : attrList) {
                    Attribute localAttr = (Attribute) attr;
                    if (localAttr.getName().equalsIgnoreCase("type")) {
                        component = localAttr.getValue() + "." + component;
                    }
                    attrMap.put(localAttr.getName(), localAttr.getValue().toString());
                }
                mbeanMap.put(component, attrMap);
            } catch (Exception e) {
                LOG.error("Unable to poll JMX for metrics.", e);
            }
        }
        return mbeanMap;
    }

    private static MBeanServerConnection createMBeanServerConn(String host, String port) throws IOException {
        String url = "service:jmx:rmi:///jndi/rmi://" + host + ":" + port + "/jmxrmi";
        JMXServiceURL serviceUrl = new JMXServiceURL(url);
        JMXConnector jmxConnector = JMXConnectorFactory.connect(serviceUrl, null);
        return  jmxConnector.getMBeanServerConnection();
    }
}
