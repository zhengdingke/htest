package com.zdingke.common.db.utils;

import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.logicalcobwebs.proxool.ProxoolException;

import com.google.common.collect.Lists;

public class DbToLocalFile {

    private static Log LOG = LogFactory.getLog(DbToLocalFile.class);

    private List<String> fetchDBData(String sql) {
        List<String> result = Lists.newArrayList();
        Connection conn = null;
        ResultSet rs;
        try {
            conn = ConnectionFactory.getConnection("log-mysql");
            if (conn == null) {
                LOG.error("get database connection failed");
                return result;
            }
            PreparedStatement statement = conn.prepareStatement(sql);
            rs = statement.executeQuery(sql);
            while (rs.next()) {
                result.add(rs.getString(1));
            }
        } catch (ClassNotFoundException | SQLException | IOException | ProxoolException e) {
            LOG.error(e.getMessage(), e);
        }
        return result;
    }
    
    private void writeToLocal(List<String> healthyNo,String file) throws IOException {
        FileWriter fw = new FileWriter(file);
        healthyNo.stream().forEach(h -> {
            try {
                fw.write(h + "\r\n");
            } catch (Exception e) {
                LOG.error(e.getMessage(), e);
            }
        });
        fw.close();
    }

    public static void main(String[] args) throws IOException {
        DbToLocalFile df = new DbToLocalFile();
        List<String> dataList = df.fetchDBData(args[0]);
        df.writeToLocal(dataList, args[1]);
    }
}
