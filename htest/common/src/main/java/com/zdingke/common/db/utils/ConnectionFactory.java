package com.zdingke.common.db.utils;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Properties;
/*import java.util.concurrent.ConcurrentHashMap;

import org.logicalcobwebs.proxool.ProxoolDataSource;
import org.logicalcobwebs.proxool.ProxoolException;*/
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.logicalcobwebs.proxool.ProxoolDataSource;
import org.logicalcobwebs.proxool.ProxoolException;

public class ConnectionFactory {

    private static ConcurrentHashMap<String, ProxoolDataSource> dataSources = new ConcurrentHashMap<String, ProxoolDataSource>();
    private static Log LOG = LogFactory.getLog(ConnectionFactory.class);

    private static ProxoolDataSource initDataSource(String alias) throws ClassNotFoundException, IOException, ProxoolException {


        Class.forName("org.logicalcobwebs.proxool.ProxoolDriver");
        Properties properties = new Properties(); 
        properties.setProperty("alias", alias);
        properties.load(ConnectionFactory.class.getResource("/conf/" + alias + ".properties").openStream());
        String driverClass = properties.getProperty("proxool.driver-class");
        String driverUrl = properties.getProperty("proxool.driver-url");

        ProxoolDataSource ds = new ProxoolDataSource();		
        ds.setDriver(driverClass); 
        ds.setDriverUrl(driverUrl);    
        ds.setUser(properties.getProperty("user")); 
        ds.setPassword(properties.getProperty("password")); 
        ds.setAlias("alias"); 
        ds.setMaximumConnectionCount(Integer.parseInt(properties.getProperty("proxool.maximum-connection-count"))); 
        ds.setMinimumConnectionCount(Integer.parseInt(properties.getProperty("proxool.minimum-connection-count"))); 
        //ds.setMaximumActiveTime(5); 

        return ds;		
    } 

    public static Connection getConnection(String alias) throws SQLException, ClassNotFoundException, IOException, ProxoolException {
        Connection conn = null; // ���������������ݿ��Connection����

        Properties properties = new Properties(); 
        properties.setProperty("alias", alias);
        properties.load(ConnectionFactory.class.getResource("/conf/" + alias + ".properties").openStream());
        //conn = getConnection(driverClass, driverUrl, properties.getProperty("user"), properties.getProperty("password"));
        ProxoolDataSource dataSource = null;
        if (dataSources.containsKey(alias)) {
            dataSource = dataSources.get(alias);
        } else {
            dataSource = initDataSource(alias);
            dataSources.put(alias, dataSource);
        }
        if (dataSource != null) {
            conn = dataSource.getConnection();
        }
        return conn;
    }

    public static Connection getConnection(String Driver,String Url,String Username,String Password) throws SQLException, ClassNotFoundException {
        Connection connection = null;
        String driver = Driver;
        Class.forName(driver);
        if (connection == null || connection.isClosed()) {
            String username = Username;
            String password = Password;
            String url = Url;

            DriverManager.setLoginTimeout(10);
            connection = DriverManager.getConnection(url, username,
                    password);
        }
        return connection;
    }

    public static void main(String[] args) {
        Connection conn = null;
        ResultSet rs;
        try {
            conn = ConnectionFactory.getConnection("log-mysql");
            if (conn == null) {
                LOG.info("get database connection failed");
                return;
            }
            String sql = "select ftenant_id,his_service_url from P_TEMP_TENANT_HISSERVICE group by his_service_url having count(1)=1;";

            PreparedStatement statement = conn.prepareStatement(sql);
            rs = statement.executeQuery(sql);
            ResultSetMetaData rsmd = rs.getMetaData();
            int columnCount = rsmd.getColumnCount();
            while (rs.next()) {
                for (int i = 1; i <= columnCount; i++) {
                    System.out.println(rs.getString(i));
                }
            }
        } catch (ClassNotFoundException | SQLException | IOException | ProxoolException e) {
            LOG.error(e.getMessage(), e);
        }
    }
}
