package com.zdingke.common.linuxtest;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.zdingke.common.hdfs.utils.HDFSUtil;

public class HDFSIOTest {
    private static final Log LOG = LogFactory.getLog(HDFSIOTest.class);

    public static void main(String[] args) throws Exception {
        InputStream in = new FileInputStream(new File(args[0]));
        boolean result = HDFSUtil.upload(in, args[0].substring(args[0].lastIndexOf("/") + 1), args[1]);
        LOG.info(result);
    }
}
