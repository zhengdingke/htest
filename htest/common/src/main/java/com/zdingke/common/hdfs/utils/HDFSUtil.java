package com.zdingke.common.hdfs.utils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.PathFilter;

import com.google.common.collect.Lists;
import com.zdingke.common.context.HadoopContext;
import com.zdingke.common.model.FileInfo;

public class HDFSUtil {

    private static final Log LOG = LogFactory.getLog(HDFSUtil.class);

    private HDFSUtil() {

    }

    public static boolean upload(InputStream in, String fileName, String dstPath) throws IOException {
        try {
            uploadFile(in, fileName, dstPath);
        } catch (IOException e) {
            LOG.error(e.getMessage(), e);
            return false;
        }
        return true;
    }

    public static boolean delete(String path) throws IOException {
        try {
            FileSystem system = FileSystem.get(HadoopContext.getConf());
            return system.delete(new Path(path.trim().toString()), false);
        } catch (IOException e) {
            LOG.error(e.getMessage(), e);
        }
        return false;
    }

    private static void uploadFile(InputStream in, String fileName, String dstPath) throws IOException {
        FileSystem system = FileSystem.get(HadoopContext.getConf());
        Path dst = new Path(dstPath);
        mkdirOnNoExist(system, dst);
        FSDataOutputStream out = system.create(new Path(dstPath, fileName));
        byte[] buffer = new byte[1024];
        try {
            int size = 0;
            while ((size = in.read(buffer)) != -1) {
                out.write(buffer, 0, size);
            }
        } finally {
            out.close();
            in.close();
        }
    }

    public static List<FileInfo> list(String path, String regex) throws IOException {
        try {
            FileSystem system = FileSystem.get(HadoopContext.getConf());
            Path p = new Path(path);
            mkdirOnNoExist(system, p);
            FileStatus[] fileStatus = FileFilter(regex, system, p);
            List<FileInfo> list = Lists.newArrayList();
            Arrays.asList(fileStatus).stream().forEach(f -> {
                list.add(new FileInfo(f.getPath().getName(), f.getLen(), f.getModificationTime()));
            });
            return list;
        } catch (IOException e) {
            LOG.error(e.getMessage(), e);
        }
        return Lists.newArrayList();
    }

    private static FileStatus[] FileFilter(String regex, FileSystem system, Path p) throws FileNotFoundException, IOException {
        FileStatus[] fileStatus = system.listStatus(p, (PathFilter) path -> regex == null ? true : path.getName().toString().contains(regex));
        return fileStatus;
    }

    private static void mkdirOnNoExist(FileSystem system, Path p) throws IOException {
        if (!system.exists(p)) {
            system.mkdirs(p);
        }
    }
}
