package com.zdingke.common.hdfs.utils;


import java.io.Serializable;
import java.util.Map;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.log4j.Logger;

/**
 * MobConfiguration
 *
 * @author wangrenzheng
 * @since 2016-11-17
 */
public class HadoopConfiguration extends Configuration implements Serializable {
    
	/** @Fields serialVersionUID: */
	  	
	private static final long serialVersionUID = -4258860640537465514L;

	public static final Logger LOG = Logger.getLogger(HadoopConfiguration.class);

    private static HadoopConfiguration conf = null;

    public static HadoopConfiguration getInstance() {
    	LOG.info("hbase.zookeeper.quorum3: starting");
        if (conf == null) {
            synchronized (HadoopConfiguration.class) {
                if (conf == null) {
                    conf = new HadoopConfiguration();
                }
                conf.init(null);
            }
        }
        
        return conf;
    }
    
    public static class SingletonHolder {
        private static final HadoopConfiguration INSTANCE = new HadoopConfiguration();  
    }

    private HadoopConfiguration() {

    }
    
    public static final HadoopConfiguration getSingleton(){
        return SingletonHolder.INSTANCE;
    }

    protected void init(Configuration aConf) {
        // 可外部配置zk和各种参数
//        this.addResource("hbase-site.xml");
        this.addResource("core-site.xml");
        this.addResource("yarn-site.xml");
        this.addResource("hdfs-site.xml");
        this.addResource("mapred-site.xml");
//        String hadoopConfDir = System.getenv("HADOOP_HOME") + "/conf";
        String hadoopConfDir = "/usr/hdp/2.3.4.0-3485/hadoop/conf";
//        String hbaseConfDir = System.getenv("HBASE_HOME") + "/conf";
//        this.addResource(new Path(hbaseConfDir + "/hbase-site.xml"));
        this.addResource(new Path(hadoopConfDir + "/core-site.xml"));
        this.addResource(new Path(hadoopConfDir + "/yarn-site.xml"));
        this.addResource(new Path(hadoopConfDir + "/hdfs-site.xml"));
        this.addResource(new Path(hadoopConfDir + "/mapred-site.xml"));

        LOG.info("Get Configuration ...");

        // copy配置
        if (aConf != null) {
            for (Map.Entry<String, String> entry : aConf) {
                this.set(entry.getKey(), entry.getValue());
            }
        }
    }

}
