package com.zdingke.common.utils;


import java.io.IOException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.ZooDefs;
import org.apache.zookeeper.ZooKeeper;
import org.apache.zookeeper.data.Stat;

public class ZkUtil {
    public static final Log LOG = LogFactory.getLog(ZkUtil.class);
    private ZooKeeper zooKeeper = null;

    public ZkUtil() {
        this.zooKeeper = connectToZK();
        if (this.zooKeeper == null) {
            throw new RuntimeException("连接ZooKeeper失败");
        }
    }

    public ZooKeeper getZooKeeper() {
        if ((this.zooKeeper == null) || (!this.zooKeeper.getState().isAlive())) {
            this.zooKeeper = connectToZK();
        }
        return this.zooKeeper;
    }

    private ZooKeeper connectToZK() {
        LOG.info("Connecting to ZooKeeper server");
        if ((this.zooKeeper != null) && (this.zooKeeper.getState().isAlive())) {
            try {
                this.zooKeeper.close();
            } catch (InterruptedException e) {
                LOG.info("连接ZooKeeper时关闭已连接ZK失败" + e.getMessage(), e);
                return null;
            }
        }
        try {
            return new ZooKeeper("zookeeper.connectString", 60 * 1000,
                    new SessionWatcher());
        } catch (IOException e) {
            LOG.info("重新连接ZooKeeper失败" + e.getMessage(), e);
        }
        return null;
    }

    class SessionWatcher implements Watcher {
        SessionWatcher() {
        }

        @Override
        public void process(WatchedEvent event) {
            if (event.getState() == Watcher.Event.KeeperState.SyncConnected) {
                if (LOG.isDebugEnabled()) {
                    LOG.debug(event.getState() + "," + event.getPath() + "," + event.getType());
                }
            } else if (event.getState() == Watcher.Event.KeeperState.Expired) {
                LOG.error("warning session Expired::" + event.toString());
                ZkUtil.this.zooKeeper = ZkUtil.this.connectToZK();
            }
        }
    }

    private String checkPath(String path) {
        if ((path != null) && (path.length() > 1)) {
            path = path.replaceAll("///", "/");
            path = path.replaceAll("//", "/");
            if ((path.length() > 1) && (path.charAt(path.length() - 1) == '/')) {
                path = path.substring(0, path.length() - 1);
            }
        }
        return path;
    }

    public void putString(String path, String data) throws InterruptedException, KeeperException {
        if (!ifExist(path)) {
            mkdirs(path);
        }
        setData(path, data);
    }

    private void setData(String path, String data) throws KeeperException, InterruptedException {
        this.zooKeeper.setData(path, data.getBytes(), -1);
    }

    public boolean ifExist(String path) throws InterruptedException {
        try {
            Stat stat = this.zooKeeper.exists(path, false);
            if (stat != null) {
                return true;
            }
        } catch (KeeperException e) {
            LOG.warn("目录不存在:" + path + ",开始创建");
        } catch (InterruptedException e) {
            throw e;
        }
        return false;
    }

    public void mkdirs(String path) throws InterruptedException {
        path = checkPath(path);
        if (path != null) {
            String tempPath = "/";
            String[] tokens = path.split("/");
            for (String token : tokens) {
                if (token.length() != 0) {
                    tempPath = tempPath + token;
                    LOG.info(tempPath);
                    try {
                        Stat stat = this.zooKeeper.exists(tempPath, false);
                        if (stat == null) {
                            LOG.info("创建目录:" + tempPath);
                            this.zooKeeper.create(tempPath, new byte[0], ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT);
                        }
                    } catch (Exception e) {
                        LOG.warn("创建目录异常:" + tempPath + "," + e.getMessage(), e);
                    }
                    tempPath = tempPath + "/";
                }
            }
        }
    }
}
