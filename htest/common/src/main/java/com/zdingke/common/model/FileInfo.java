package com.zdingke.common.model;

public class FileInfo {
    private String fileName;
    private long length;
    private long creatTime;

    public FileInfo(String fileName, long length, long creatTime) {
        this.fileName = fileName;
        this.length = length;
        this.creatTime = creatTime;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public long getLength() {
        return length;
    }

    public void setLength(long length) {
        this.length = length;
    }

    public long getCreatTime() {
        return creatTime;
    }

    public void setCreatTime(long creatTime) {
        this.creatTime = creatTime;
    }

}
