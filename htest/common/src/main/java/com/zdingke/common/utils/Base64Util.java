package com.zdingke.common.utils;

import java.io.IOException;

/**
 * 
 * Base64����빤��?
 * @author Cecilia
 *
 */
public class Base64Util {
	
	/**
	 * ���ַ�������base64����
	 * @param s
	 * @return
	 */
	public static String getBASE64(String s) {
		if (s == null)
			return null;
		return (new sun.misc.BASE64Encoder()).encode(s.getBytes());
	}
	
	/**
	 * ���ַ�������base64����
	 * @param str
	 * @return
	 * @throws IOException 
	 */
	public static String decode(String str) throws IOException {
		byte[] bt = null;
		sun.misc.BASE64Decoder decoder = new sun.misc.BASE64Decoder();
		bt = decoder.decodeBuffer(str);
		return new String(bt);
	}

}
