package com.zdingke.common.hive.utils;

public class TableStaticInfo {

    private String tableName;
    private String comment;
    private String createTime;
    private int partitionNum;
    private int numFiles;
    private int numRows;
    private long totalSize;
    private long rawDataSize;

    public TableStaticInfo() {
        super();
    }

    public TableStaticInfo(String tableName, String comment, String createTime, int partitionNum, int numFiles, int numRows, long totalSize, long rawDataSize) {
        super();
        this.tableName = tableName;
        this.comment = comment;
        this.createTime = createTime;
        this.partitionNum = partitionNum;
        this.numFiles = numFiles;
        this.numRows = numRows;
        this.totalSize = totalSize;
        this.rawDataSize = rawDataSize;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public int getPartitionNum() {
        return partitionNum;
    }

    public void setPartitionNum(int partitionNum) {
        this.partitionNum = partitionNum;
    }

    public int getNumFiles() {
        return numFiles;
    }

    public void setNumFiles(int numFiles) {
        this.numFiles = numFiles;
    }

    public int getNumRows() {
        return numRows;
    }

    public void setNumRows(int numRows) {
        this.numRows = numRows;
    }

    public long getTotalSize() {
        return totalSize;
    }

    public void setTotalSize(long totalSize) {
        this.totalSize = totalSize;
    }

    public long getRawDataSize() {
        return rawDataSize;
    }

    public void setRawDataSize(long rawDataSize) {
        this.rawDataSize = rawDataSize;
    }

    @Override
    public String toString() {
        return "TableStaticInfo [tableName=" + tableName + ", comment=" + comment + ", createTime=" + createTime + ", partitionNum=" + partitionNum
                + ", numFiles=" + numFiles + ", numRows=" + numRows + ", totalSize=" + totalSize + ", rawDataSize=" + rawDataSize + "]";
    }

}
