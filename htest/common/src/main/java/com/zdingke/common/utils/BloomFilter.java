package com.zdingke.common.utils;

import java.util.BitSet;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class BloomFilter {

    private static final Log LOG = LogFactory.getLog(BloomFilter.class);

    private static final int DEFAULT_SIZE = 1 << 26;
    private static final int[] seeds = new int[] { 131, 1313, 13131 };
    private BitSet bits = new BitSet(DEFAULT_SIZE);
    private WeightHash[] func = new WeightHash[seeds.length];

    public BloomFilter() {
        initHashFunc(DEFAULT_SIZE);
    }

    public BloomFilter(int lenNum) {
        bits = new BitSet(1 << lenNum);
        initHashFunc(1 << lenNum);
    }

    private void resetBit(int size) {
        ResetThread reset = new ResetThread(size);
        new Thread(reset).start();
    }

    private void initHashFunc(int len) {
        for (int i = 0; i < seeds.length; i++) {
            func[i] = new WeightHash(len, seeds[i]);
        }
    }

    public void add(String value) {
        Stream.of(func).forEach(f -> {
            bits.set(f.hash(value));
        });
    }

    public boolean contains(String value) {
        if (StringUtils.isBlank(value)) {
            return false;
        }
        boolean result = true;
        for(WeightHash f:func){
            result &= bits.get(f.hash(value));
        }
        return result;
    }

    public int getBitSize() {
        return bits.size();
    }

    public int getCardinality() {
        return bits.cardinality();
    }

    public class WeightHash {
        private int cap;
        private int seed;

        public WeightHash(int cap, int seed) {
            this.cap = cap;
            this.seed = seed;
        }

        public int hash(String value) {
            int result = 0;

            String md5 = StringUtil.toMD5(value);
            int len = md5.length();
            for (int i = 0; i < len; i++) {
                result = seed * result + md5.charAt(i);
            }
            return (cap - 1) & result;
        }
    }

    public class ResetThread implements Runnable {
        private int size;

        public ResetThread(int size) {
            this.size = size;
        }

        @Override
        public void run() {
            while (true) {
                try {
                    Thread.sleep(60 * 60 * 60 * 1000L);
                    bits = new BitSet(size);
                } catch (Exception e) {
                    LOG.error("resetbit error!");
                }
            }
        }
    }
}
