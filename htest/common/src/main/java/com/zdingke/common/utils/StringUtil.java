package com.zdingke.common.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class StringUtil {

    private static final Log LOG = LogFactory.getLog(StringUtil.class);
    
    /*
     * 首字母转成大写
     */
    public static String tranFirstCharUpper(String fildeName) {
        byte[] items = fildeName.getBytes();
        items[0] = (byte) ((char) items[0] - 'a' + 'A');
        return new String(items);
    }

    public static String toMD5(String str) {
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            LOG.error(e);
        }
        byte[] md5 = md.digest(str.getBytes());
        return new String(Base64.getEncoder().encode(md5));
    }
}
