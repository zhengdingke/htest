package com.zdingke.common.context;

import java.io.IOException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hive.conf.HiveConf;
import org.apache.hadoop.mapreduce.util.ConfigUtil;
import org.apache.hive.hcatalog.common.HCatConstants;
import org.apache.hive.hcatalog.common.HCatUtil;

public class HadoopContext {
    private static final Log LOG = LogFactory.getLog(HadoopContext.class);
    private static Configuration conf = null;
    private static HiveConf hiveConf = null;
    private static int pid = -1;
    private static String hostname = "";
    private static String pidAtHostname = "";
    private static String canonicalHostName = "";
    static {
        ConfigUtil.loadResources();

        Configuration.addDefaultResource("core-site.xml");
        Configuration.addDefaultResource("hdfs-site.xml");
        Configuration.addDefaultResource("yarn-site.xml");
        Configuration.addDefaultResource("mapred-site.xml");
        Configuration.addDefaultResource("hbase-site.xml");
        Configuration.addDefaultResource("hive-site.xml");
        Configuration.addDefaultResource("tez-site.xml");

        // // 初始化PID
        // String processName =
        // java.lang.management.ManagementFactory.getRuntimeMXBean().getName();
        // pid = Integer.parseInt(processName.split("@")[0]);
        // // 初始化HOSTNAME
        // try {
        // hostname = (InetAddress.getLocalHost()).getHostName();
        // canonicalHostName =
        // (InetAddress.getLocalHost()).getCanonicalHostName();
        // } catch (UnknownHostException e) {
        // LOG.warn("get hostname failed:" + e.getMessage(), e);
        // hostname = "unknown";
        // }
        // pidAtHostname = pid + "@" + hostname;
        // LOG.info("--------pid@hostname=" + pidAtHostname);

        conf = new Configuration();
        try {
            hiveConf = HCatUtil.getHiveConf(HadoopContext.getConf());
            LOG.info("******hiveclientcache****:" + hiveConf.getBoolean(HCatConstants.HCAT_HIVE_CLIENT_DISABLE_CACHE, false));
        } catch (IOException e) {
            throw new RuntimeException("init hiveConf failed:" + e.getMessage(), e);
        }
        // try {
        // ZooKeeperWatcher zookeeper = new
        // ZooKeeperWatcher(OdpContext.getConf(), OdpContext.getPidAtHostname(),
        // null, true);
        // OdpContext.setZooKeeperWatcher(zookeeper);
        // } catch (IOException e) {
        // throw new RuntimeException("Connect to zookeeper failed:" +
        // e.getMessage(), e);
        // }
    }

    private HadoopContext() {

    }


    public static Configuration getConf() {
        return conf;
    }

    public static HiveConf getHiveConf() {
        return hiveConf;
    }

    public static int getPid() {
        return pid;
    }

    public static String getHostname() {
        return hostname;
    }

    public static String getCanonicalHostName() {
        return canonicalHostName;
    }

    public static String getPidAtHostname() {
        return pidAtHostname;
    }
}
