package com.zdingke.common.hive.utils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.hive.metastore.HiveMetaStoreClient;
import org.apache.hadoop.hive.metastore.api.FieldSchema;
import org.apache.hadoop.hive.metastore.api.Partition;
import org.apache.hadoop.hive.metastore.api.Table;
import org.apache.thrift.TException;

import com.zdingke.common.context.HadoopContext;

public class HiveMetaUtil {

    private static final Log LOG = LogFactory.getLog(HiveMetaUtil.class);
    private static final String NUMFILES = "numFiles";
    private static final String NUMROWS = "numRows";
    private static final String TOTALSIZE = "totalSize";
    private static final String ROWDATASIZE = "rawDataSize";
    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    private HiveMetaUtil() {

    }

    private static List<String> listTable(String db) throws TException {
        HiveMetaStoreClient client = new HiveMetaStoreClient(HadoopContext.getHiveConf());
        return client.getAllTables(db);
    }

    private static List<TableFieldSchema> getTable(String db, String tableName) throws TException {
        HiveMetaStoreClient client = new HiveMetaStoreClient(HadoopContext.getHiveConf());
        Table table = client.getTable(db, tableName);
        List<FieldSchema> partitionKeys = table.getPartitionKeys();
        List<FieldSchema> field = client.getFields(db, tableName);
        List<TableFieldSchema> list = new ArrayList<TableFieldSchema>();
        for (FieldSchema p : partitionKeys) {
            TableFieldSchema tfs = new TableFieldSchema(p.getName(), p.getType(), p.getComment(), TableFieldSchema.FieldType.PARTITION);
            list.add(tfs);
        }
        for (FieldSchema p : field) {
            TableFieldSchema tfs = new TableFieldSchema(p.getName(), p.getType(), p.getComment(), TableFieldSchema.FieldType.NORMAL);
            list.add(tfs);
        }

        return list;
    }

    private static List<String> getTablePar(String db, String tableName) throws TException {
        HiveMetaStoreClient client = new HiveMetaStoreClient(HadoopContext.getHiveConf());
        List<String> pNameList = client.listPartitionNames(db, tableName, (short) -1);
        return pNameList;
    }

    public static List<String> getTablePartitions(String tenant, String tableName) {
        try {
            return getTablePar(tenant, tableName);
        } catch (TException e) {
            LOG.error(e.getMessage(), e);
        }
        return new ArrayList<String>();
    }

    public static List<TableFieldSchema> getTableSchema(String tenant, String tableName) {
        try {
            return getTable(tenant, tableName);
        } catch (TException e) {
            LOG.error(e.getMessage(), e);
        }
        return new ArrayList<TableFieldSchema>();
    }

    public static List<String> listTables(String tenant) {
        try {
            return listTable(tenant);
        } catch (TException e) {
            LOG.error(e.getMessage(), e);
        }
        return new ArrayList<String>();
    }

    public static List<TableStaticInfo> listTableStaticInfos(String tenant) {
        try {
            return getTables(tenant);
        } catch (TException e) {
            LOG.error(e.getMessage(), e);
        }
        return new ArrayList<TableStaticInfo>();
    }

    private static List<TableStaticInfo> getTables(String db) throws TException {
        HiveMetaStoreClient client = new HiveMetaStoreClient(HadoopContext.getHiveConf());
        List<String> tables = client.getAllTables(db);
        List<TableStaticInfo> list = new ArrayList<TableStaticInfo>();
        for (String tableName : tables) {
            Table table = client.getTable(db, tableName);
            String createTime = sdf.format(new Date(table.getCreateTime() * 1000l));
            List<String> pNameList = client.listPartitionNames(db, tableName, (short) -1);
            List<Map<String, String>> partitionInfos = getParameters(db, tableName, pNameList, client);
            list.add(paserToBean(partitionInfos, tableName, "", createTime));
        }
        return list;
    }

    private static TableStaticInfo paserToBean(List<Map<String, String>> partitionInfos, String tableName, String comment, String createTime) {
        int numFiles = 0;
        int numRows = 0;
        long totalSize = 0;
        long rawDataSize = 0;
        for (Map<String, String> map : partitionInfos) {
            numFiles += Integer.parseInt(map.getOrDefault(NUMFILES, 0 + ""));
            numRows += Integer.parseInt(map.getOrDefault(NUMROWS, 0 + ""));
            totalSize += Long.parseLong(map.getOrDefault(TOTALSIZE, 0 + ""));
            rawDataSize += Long.parseLong(map.getOrDefault(ROWDATASIZE, 0 + ""));
        }
        return new TableStaticInfo(tableName, comment, createTime, partitionInfos.size(), numFiles, numRows, totalSize, rawDataSize);
    }

    private static List<Map<String, String>> getParameters(String dbName, String tableName, List<String> pNameList, HiveMetaStoreClient client)
            throws TException {
        List<Map<String, String>> mapList = new ArrayList<>();
        if (pNameList.isEmpty()) {
            Table table = client.getTable(dbName, tableName);
            mapList.add(table.getParameters());
        } else {
            List<Partition> ps = client.listPartitions(dbName, tableName, (short) -1);
            for (int i = 0; i < ps.size(); i++) {
                Partition p = ps.get(i);
                mapList.add(p.getParameters());
            }
        }
        return mapList;
    }
}
