package com.zdingke.common.hdfs.utils;

import java.io.IOException;

import org.apache.hadoop.security.UserGroupInformation;


public class EasyKerberos {
    public  void getKerberosToken(String principle, String keytabPath){
        HadoopConfiguration conf = HadoopConfiguration.getInstance();
        try {
            UserGroupInformation.setConfiguration(conf);
            UserGroupInformation.loginUserFromKeytab(principle, keytabPath);
        } catch (IOException e) {
           e.printStackTrace();
       }
    }
    
    public static void main(String[] args) {
        EasyKerberos easy = new EasyKerberos();
        easy.getKerberosToken("xjslzx/xjznyp.wgzx@WGZX", "/home/xjslzx/xjslzxhdp.keytab");
    }
}
