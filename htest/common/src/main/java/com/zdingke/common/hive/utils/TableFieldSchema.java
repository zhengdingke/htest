package com.zdingke.common.hive.utils;

public class TableFieldSchema {

    private String name; // required
    private String type; // required
    private String comment; // required
    private FieldType fieldType;

    public enum FieldType {
        PARTITION, // 分区字段
        NORMAL// 普通字段
    }

    public TableFieldSchema() {
        super();
    }

    public TableFieldSchema(String name, String type, String comment, FieldType fieldType) {
        super();
        this.name = name;
        this.type = type;
        this.comment = comment;
        this.fieldType = fieldType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public FieldType getFieldType() {
        return fieldType;
    }

    public void setFieldType(FieldType fieldType) {
        this.fieldType = fieldType;
    }

}
