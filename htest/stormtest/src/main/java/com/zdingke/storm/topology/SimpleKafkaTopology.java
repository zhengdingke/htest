package com.zdingke.storm.topology;

import java.io.IOException;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.storm.Config;
import org.apache.storm.StormSubmitter;
import org.apache.storm.generated.AlreadyAliveException;
import org.apache.storm.generated.AuthorizationException;
import org.apache.storm.generated.InvalidTopologyException;
import org.apache.storm.kafka.BrokerHosts;
import org.apache.storm.kafka.KafkaSpout;
import org.apache.storm.kafka.SpoutConfig;
import org.apache.storm.kafka.StringScheme;
import org.apache.storm.kafka.ZkHosts;
import org.apache.storm.spout.SchemeAsMultiScheme;
import org.apache.storm.topology.TopologyBuilder;

import com.zdingke.storm.bolt.WordCountBolt;

public class SimpleKafkaTopology {

    private static final Log log = LogFactory.getLog(SimpleKafkaTopology.class);

    public static String config = "app-logging.properties";

    public static void main(String[] args) throws IOException, AlreadyAliveException, InvalidTopologyException, AuthorizationException {
        Properties properties = new Properties();
        properties.load(SimpleKafkaTopology.class.getResource("/" + config).openStream());
        String hosts = properties.getProperty("spout.brokerHosts");
        BrokerHosts brokerHosts = new ZkHosts(hosts);
        String topicName = properties.getProperty("spout.topicName");
        SpoutConfig spoutConfig = new SpoutConfig(brokerHosts, topicName, "/storm", "app-logging-kafkaSpout");

        // spoutConfig.scheme = new SchemeAsMultiScheme(new StringScheme());
        spoutConfig.scheme = new SchemeAsMultiScheme(new StringScheme());
        spoutConfig.ignoreZkOffsets = false;
        spoutConfig.fetchMaxWait = Integer.parseInt(properties.getProperty("spout.config.fetchMaxWait"));
        spoutConfig.fetchSizeBytes = Integer.parseInt(properties.getProperty("spout.config.fetchSizeBytes"));
        KafkaSpout kafkaSpout = new KafkaSpout(spoutConfig);

        TopologyBuilder builder = new TopologyBuilder();
        String inputSpout = "topology.input-kafka-spout";
        String wordcountBolt = "topology.wordcount-bolt";
        builder.setSpout(inputSpout, kafkaSpout, Integer.parseInt(properties.getProperty(inputSpout)));
        builder.setBolt(wordcountBolt, new WordCountBolt(), Integer.parseInt(properties.getProperty(wordcountBolt)));

        Config conf = new Config();
        String workers = properties.getProperty("topology.workers");
        conf.setNumWorkers(Integer.parseInt(workers));
        conf.setMaxSpoutPending(5000);
        conf.setMessageTimeoutSecs(360);
        StormSubmitter.submitTopology("app-logging-topology", conf, builder.createTopology());
    }
}
