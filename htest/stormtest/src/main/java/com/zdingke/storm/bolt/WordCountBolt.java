package com.zdingke.storm.bolt;

import java.io.IOException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.storm.topology.BasicOutputCollector;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseBasicBolt;
import org.apache.storm.tuple.Tuple;

import com.zdingke.storm.model.LogEntry;

public class WordCountBolt extends BaseBasicBolt {

    private static final Log logger = LogFactory.getLog(WordCountBolt.class);

    @Override
    public void execute(Tuple input, BasicOutputCollector collector) {
        String message = input.getValue(0).toString();
        try {
            LogEntry.FromLog(message);
        } catch (IOException e) {
            logger.error("日志解码异常", e);
            return;
        }
        System.out.println("dingke");
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {

    }

}
