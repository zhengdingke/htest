package com.zdingke.storm.model;

import java.io.IOException;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.io.xml.DomDriver;

@XStreamAlias("log")
public class LogEntry {
	
	private String host;
	private String timestamp;
	private String level;
	private String thread;
	private String message;
	private String tenantId;
	private String categoryName;
	
	public String getHost() {
		return host;
	}
	public String getTimestamp() {
		return timestamp;
	}
	public String getLevel() {
		return level;
	}
	public String getThread() {
		return thread;
	}
	public String getMessage() {
		return message;
	}
	public void setHost(String host) {
		this.host = host;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	public void setLevel(String level) {
		this.level = level;
	}
	public void setThread(String thread) {
		this.thread = thread;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getTenantId() {
		return tenantId;
	}
	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	
	public static int getXmlLength(String log) {
        char[] len = new char[10];
        log.getChars(0, 10, len, 0);
        return Integer.parseInt(String.valueOf(len));
    }

    public static String getXmlStr(String log, int len) {
        char[] xmlchars = new char[len];
        log.getChars(10, 10 + len, xmlchars, 0);
        return String.valueOf(xmlchars);
    }

    public static String getMsgStr(String log, int len) {
        char[] msgchars = new char[log.length() - len - 10];
        log.getChars(10 + len, log.length(), msgchars, 0);
        return String.valueOf(msgchars);
    }
	
	public static LogEntry FromLog(String message) throws IOException {
		LogEntry log = null;
		XStream xStream = new XStream(new DomDriver("utf-8"));
		xStream.processAnnotations(LogEntry.class);
		if (message.startsWith("<log>")) {
			log = (LogEntry) xStream.fromXML(message);
            log.setMessage("test");
		} else {								
			int length = getXmlLength(message);
			String xml = getXmlStr(message, length);
			String msg = getMsgStr(message, length);			
			log = (LogEntry) xStream.fromXML(xml);
			log.setMessage(msg);
		}
		return log;
	}

}
