package com.zdingke.jstorm.topology;

import java.io.IOException;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import storm.kafka.BrokerHosts;
import storm.kafka.KafkaSpout;
import storm.kafka.SpoutConfig;
import storm.kafka.StringScheme;
import storm.kafka.ZkHosts;
import backtype.storm.Config;
import backtype.storm.StormSubmitter;
import backtype.storm.generated.AlreadyAliveException;
import backtype.storm.generated.InvalidTopologyException;
import backtype.storm.spout.SchemeAsMultiScheme;
import backtype.storm.topology.TopologyBuilder;

import com.zdingke.jstorm.bolt.WordCountBolt;

/*
 * 0.9.2 storm-kafka
 */
public class SimpleKafkaTopology2 {

    private static final Log log = LogFactory.getLog(SimpleKafkaTopology2.class);

    public static String config = "app-logging2.properties";

    public static void main(String[] args) throws IOException, AlreadyAliveException, InvalidTopologyException {
        Properties properties = new Properties();
        properties.load(SimpleKafkaTopology2.class.getResource("/" + config).openStream());

        String hosts = properties.getProperty("spout.brokerHosts");
        BrokerHosts brokerHosts = new ZkHosts(hosts);
        String topicName = properties.getProperty("spout.topicName");
        SpoutConfig spoutConfig = new SpoutConfig(brokerHosts, topicName, "/jstorm", "app-logging-kafkaSpout");

        // 该Topology因故障停止处理，下次正常运行时是否从Spout对应数据源Kafka中的该订阅Topic的起始位置开始读取
        spoutConfig.forceFromStart = false;
        // spoutConfig.scheme = new SchemeAsMultiScheme(new StringScheme());
        spoutConfig.scheme = new SchemeAsMultiScheme(new StringScheme());
        spoutConfig.fetchMaxWait = Integer.parseInt(properties.getProperty("spout.config.fetchMaxWait"));
        spoutConfig.fetchSizeBytes = Integer.parseInt(properties.getProperty("spout.config.fetchSizeBytes"));
        KafkaSpout kafkaSpout = new KafkaSpout(spoutConfig);

        TopologyBuilder builder = new TopologyBuilder();
        String inputSpout = "topology.input-kafka-spout";
        String wordcountBolt = "topology.wordcount-bolt";
        builder.setSpout(inputSpout, kafkaSpout, 1);
        builder.setBolt(wordcountBolt, new WordCountBolt(), 1);

        Config conf = new Config();
        conf.setNumWorkers(1);
        conf.setMaxSpoutPending(5000);
        conf.setMessageTimeoutSecs(360);
        conf.put("spout.single.thread", true);
        StormSubmitter.submitTopology("test1", conf, builder.createTopology());
    }
}
