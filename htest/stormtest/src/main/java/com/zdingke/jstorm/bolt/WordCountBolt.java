package com.zdingke.jstorm.bolt;

import java.io.IOException;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import backtype.storm.task.TopologyContext;
import backtype.storm.topology.IBasicBolt;

import com.zdingke.storm.model.LogEntry;

public class WordCountBolt implements IBasicBolt {

    private static final Log logger = LogFactory.getLog(WordCountBolt.class);

    @Override
    public void declareOutputFields(backtype.storm.topology.OutputFieldsDeclarer declarer) {
        // TODO Auto-generated method stub

    }

    @Override
    public Map<String, Object> getComponentConfiguration() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void prepare(Map stormConf, TopologyContext context) {

    }

    @Override
    public void execute(backtype.storm.tuple.Tuple input, backtype.storm.topology.BasicOutputCollector collector) {
        String message = input.getValue(0).toString();
        try {
            LogEntry log = LogEntry.FromLog(message);
            logger.info(log.toString());
        } catch (IOException e) {
            logger.error("日志解码异常", e);
            return;
        }

    }

    @Override
    public void cleanup() {
        // TODO Auto-generated method stub

    }

}
