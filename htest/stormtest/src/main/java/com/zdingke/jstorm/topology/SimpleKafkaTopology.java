package com.zdingke.jstorm.topology;

import java.io.IOException;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import backtype.storm.Config;
import backtype.storm.StormSubmitter;
import backtype.storm.generated.AlreadyAliveException;
import backtype.storm.generated.InvalidTopologyException;
import backtype.storm.topology.TopologyBuilder;

import com.alibaba.jstorm.kafka.KafkaSpout;
import com.alibaba.jstorm.kafka.KafkaSpoutConfig;
import com.zdingke.jstorm.bolt.WordCountBolt;

/*
 * 2.1.1 storm-kafka
 */
public class SimpleKafkaTopology {

    private static final Log log = LogFactory.getLog(SimpleKafkaTopology.class);

    public static String config = "app-logging.properties";

    public static void main(String[] args) throws IOException, AlreadyAliveException, InvalidTopologyException {
        Properties properties = new Properties();
        properties.load(SimpleKafkaTopology.class.getResource("/" + config).openStream());
        System.out.println(properties.get("kafka.zookeeper.hosts"));
        KafkaSpoutConfig spoutConfig = new KafkaSpoutConfig(properties);
        KafkaSpout kafkaSpout = new KafkaSpout(spoutConfig);

        TopologyBuilder builder = new TopologyBuilder();
        String inputSpout = "topology.input-kafka-spout";
        String wordcountBolt = "topology.wordcount-bolt";
        builder.setSpout(inputSpout, kafkaSpout, 1);
        builder.setBolt(wordcountBolt, new WordCountBolt(), 1);

        Config conf = new Config();
        conf.setNumWorkers(1);
        conf.setMaxSpoutPending(5000);
        conf.setMessageTimeoutSecs(360);
        conf.put("spout.single.thread", true);
        StormSubmitter.submitTopology("test4", conf, builder.createTopology());
    }
}
