package com.zdingke.xml.test;

public class people1 {

    private String name;
    private String age;
    private String date;
    private String address;
    private String contry;

    public people1(String name, String age, String date, String address, String contry) {
        this.name = name;
        this.age = age;
        this.date = date;
        this.address = address;
        this.contry = contry;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getContry() {
        return contry;
    }

    public void setContry(String contry) {
        this.contry = contry;
    }

}
