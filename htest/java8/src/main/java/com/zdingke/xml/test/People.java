package com.zdingke.xml.test;

public class People {

    private String name;
    private String age;
    private String date;
    private String address;
    private String contry;
    private String host;
    private String message;

    public People(String name, String age, String date, String address, String contry, String host, String message) {
        this.name = name;
        this.age = age;
        this.date = date;
        this.address = address;
        this.contry = contry;
        this.host = host;
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getContry() {
        return contry;
    }

    public void setContry(String contry) {
        this.contry = contry;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

}
