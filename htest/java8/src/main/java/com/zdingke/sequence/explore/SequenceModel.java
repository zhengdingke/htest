package com.zdingke.sequence.explore;

public class SequenceModel {

    private String mark;
    private String seqStr;

    public SequenceModel(String mark, String seqStr) {
        this.mark = mark;
        this.seqStr = seqStr;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    public String getSeqStr() {
        return seqStr;
    }

    public void setSeqStr(String seqStr) {
        this.seqStr = seqStr;
    }

}
