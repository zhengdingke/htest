package com.zdingke.ikexpression;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import org.wltea.expression.ExpressionEvaluator;
import org.wltea.expression.datameta.Variable;

public class IkExpressionUtil {
    /** 
     * Description: 将公式中的参数提取出来 
     * @param expression 公式 
     * @return List<变量> 
     */  
    public static List<String> expressionAnalysis(String expression){  
        String[] variableArrayTemp= new String[100];  
        List<String> variableArray= new ArrayList<String>();  
        //先将表达式中的符号用逗号替换  
        expression=expression.replaceAll("[+]", ";");  
        expression=expression.replaceAll("[-]", ";");  
        expression=expression.replaceAll("[*]", ";");  
        expression=expression.replaceAll("[/]", ";");  
        expression=expression.replaceAll("[(]", ";");  
        expression=expression.replaceAll("[)]", ";");  
        expression=expression.replaceAll("[>]", ";");  
        expression=expression.replaceAll("[<]", ";");  
        expression=expression.replaceAll("[==]",";");  
        expression=expression.replaceAll("[=]", ";");  
        expression=expression.replaceAll("[!]", ";");  
        expression=expression.replaceAll("[|]", ";");  
        expression=expression.replaceAll("[&]", ";");  
        expression=expression.replaceAll("[\"]",";");  
        expression=expression.replaceAll(" ", "");  
        variableArrayTemp= expression.split(";");//以逗号分割  
        for (int i = 0; i < variableArrayTemp.length; i++) {  
            //保存以字母开头,并且不等于true和false的  
            String temp = variableArrayTemp[i].trim();  
            if(startsWithLetter(temp)){  
                if(!"true".equals(temp)&&!"false".equals(temp)){  
                    variableArray.add(temp);  
                }  
            }  
        }  
        //去除变量相等的  
        Set<String[]> vars = new HashSet<String[]>();  
        for(Iterator<String> iter = variableArray.iterator();iter.hasNext();){  
            String temp = iter.next();  
            boolean b = false;  
            for(String[] s:vars){  
                if(s[0].equals(temp)){  
                    b = true;  
                    break;  
                }  
            }  
            if(b){  
                iter.remove();  
            }else{  
                vars.add(new String[]{temp});  
            }  
        }  
        return variableArray;  
    }  
    /** 
     * Description: 是否以字母打头 
     * @param s 
     * @return String 
     */  
    private static boolean startsWithLetter(String s) {  
        return Pattern.compile("^[A-Za-z]").matcher(s).find();  
    }  
    /** 
     * Description: 解析表达式 
     * @param expression 
     * @param map 
     * @return boolean 
     */  
    public static Boolean evaluate(String expression,Map map){  
        List<Variable> variables = new ArrayList<Variable>();  
        List<String> varList = expressionAnalysis(expression);  
        for (String str : varList) {  
            variables.add(Variable.createVariable(str, map.get(str)));  
        }  
        //解析表达式  
                expression = expression.replaceAll(" ","");  
        Object b = ExpressionEvaluator.evaluate(expression, variables);  
        return Boolean.valueOf(b.toString());  
    }  
    /*****************main************************/  
    public static void main(String[] args) {  
        String expression = "D_DAY_ID==\"20170727\"";  
        //解析出表达式中的变量  
        List<String> varList = expressionAnalysis(expression);  
        for (String string : varList) {  
            System.out.println("变量名称："+string);  
        }  
        //解析表达式的值  
        Map<String, Object> map = new HashMap<String, Object>();  
        map.put("D_DAY_ID", "\"20170727\"");  
        boolean bb = evaluate(expression,map);  
        System.out.println(bb);  
    }  
}
