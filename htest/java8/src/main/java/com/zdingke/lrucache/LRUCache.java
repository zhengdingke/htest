package com.zdingke.lrucache;

import java.util.Hashtable;

public class LRUCache {

    private Hashtable<String, Entry> nodes = new Hashtable<String, Entry>();
    private Entry head;
    private Entry tail;
    private int cacheSize;

    public LRUCache(int cacheSize) {
        this.cacheSize = cacheSize;
        head.next = tail;
        tail.prev = head;
    }

    public void put(String key,Object value){
        Entry node =  nodes.get(key);
        if(node==null){
            if(nodes.size()>=cacheSize){//缓存已满，清理最后一个node
                removeLast();
            }
            node = new Entry();
            node.key = key;
            node.value = value;
            moveToHeadIfNotExist(node);
        }else{
            moveToHeadIfExist(node);
        }
    }

    private void moveToHeadIfExist(Entry node) {
        if(node == head){
            return ;
        }
        if(node == tail){
            tail = node.prev;
            tail.next = null;

            node.prev = null;
            node.next = head;
            head = node;
        }
        if(node != head && node != tail){
            node.prev.next = node.next;
            node.next.prev = node.prev;

            node.prev = null;
            node.next = head;
            head = node;
        }
    }

    public Object get(String key){
        Entry node = nodes.get(key);  
        if(node==null) {
            return null;
        }
        moveToHeadIfExist(node);  
        return node.value;  
    }

    public void moveToHeadIfNotExist(Entry node){
        node.prev = null;
        node.next = head;
        head = node;
    }

    public void removeLast(){
        nodes.remove(tail.key);
        tail = tail.prev;
        tail.next = null;
    }

    public class Entry{
        Entry prev;
        Entry next;
        Object key;
        Object value;
    }
}
