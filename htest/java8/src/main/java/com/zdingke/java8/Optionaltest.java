package com.zdingke.java8;


import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.junit.Test;

import com.google.common.collect.Lists;

public class Optionaltest {

    @Test
    public void testOptional() {
       Optional<Object> trueOptional =  Optional.ofNullable(null);
        if (!trueOptional.isPresent()) {
            System.out.println("null");
        }
        trueOptional.get();

    }

    @Test
    public void testStreamPartitionBy() {
        List<String> list = Lists.newArrayList();
        list.add("dingke");
        list.add("abc");
        list.add("123");
        Map<Object, List<String>> map = list.stream().collect(Collectors.groupingBy(j -> j.contains("dingke")));
        map.entrySet().stream().forEach(m -> {
            System.out.println(m.getKey());
            System.out.println(m.getValue());
        });
    }
}
