package com.zdingke.jmx.test;

import javax.management.MBeanServerConnection;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;

public class JMXConnectTest {

    public static void main(String[] args) {
        try {
            JMXServiceURL address = new JMXServiceURL("service:jmx:rmi:///jndi/rmi://master2:55555/jmxrmi");
            JMXConnector connector = JMXConnectorFactory.connect(address);
            MBeanServerConnection mbs = connector.getMBeanServerConnection();
            System.out.println(mbs.getMBeanCount());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
