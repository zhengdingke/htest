package com.zdingke.strategy.basic.positive;

/**
 * @Author zhengdingke
 * @Date : 2020-12-08 14:47
 * @Description: 一般策略模式
 */
public interface Strategy {
    public void algorithmInterface();
}
