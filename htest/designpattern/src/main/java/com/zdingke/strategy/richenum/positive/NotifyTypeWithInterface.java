package com.zdingke.strategy.richenum.positive;

/**
 * @Author zhengdingke
 * @Date : 2020-12-08 13:57
 * @Description:策略设计模式优化（充血枚举类型）解决if-else问题
 */
public enum NotifyTypeWithInterface {
    email("邮件",NotifyMechanismInterface.byEmail()),
    wechat("微信",NotifyMechanismInterface.byWechat());

    String memo;
    NotifyMechanismInterface notifyMechanism;

    private NotifyTypeWithInterface(String memo, NotifyMechanismInterface notifyMechanism){
        this.memo=memo;
        this.notifyMechanism=notifyMechanism;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public NotifyMechanismInterface getNotifyMechanism() {
        return notifyMechanism;
    }

    public void setNotifyMechanism(NotifyMechanismInterface notifyMechanism) {
        this.notifyMechanism = notifyMechanism;
    }
}
