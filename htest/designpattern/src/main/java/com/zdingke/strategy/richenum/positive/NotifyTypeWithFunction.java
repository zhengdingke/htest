package com.zdingke.strategy.richenum.positive;


import java.util.function.Function;

/**
 * @Author zhengdingke
 * @Date : 2020-12-08 13:57
 * @Description:只是要对枚举值做个简单的计算获得某种 flag 标记，那就没必要把计算逻辑抽象成 NotifyMechanismInterface 那样的接口；
 * 这时就可以在枚举类型中增加 static function 封装简单的计算逻辑
 */

public enum NotifyTypeWithFunction {
    email("邮件", (str)->{
        return "邮件";
    }),
    wechat("微信",(str)->{
        return "微信";
    });

    private String memo;
    private Function<String,String> notifyService;

    private NotifyTypeWithFunction(String memo, Function notifyService){
        this.memo=memo;
        this.notifyService=notifyService;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public Function<String, String> getNotifyService() {
        return notifyService;
    }

    public void setNotifyService(Function<String, String> notifyService) {
        this.notifyService = notifyService;
    }
}
