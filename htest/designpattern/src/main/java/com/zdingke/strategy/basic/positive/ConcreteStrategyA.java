package com.zdingke.strategy.basic.positive;

/**
 * @Author Administrator
 * @Date : 2020-12-08 14:50
 * @Description:
 */
public class ConcreteStrategyA implements Strategy{
    @Override
    public void algorithmInterface() {
        System.out.println("实现具体算法A");
    }
}
