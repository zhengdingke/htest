package com.zdingke.strategy.basic.positive;

/**
 * @Author Administrator
 * @Date : 2020-12-08 14:52
 * @Description:
 */
public class Context {
    public Strategy strategy;

    public Context(Strategy strategy) {
        this.strategy = strategy;
    }

    public void contextInterface(){
        strategy.algorithmInterface();
    }
}
