package com.zdingke.strategy.richenum.negative;

/**
 * @Author zhengdingke
 * @Date : 2020-12-15 11:26
 * @Description:消息通知方式几口
 */
public class NotifyService {

    public String notify(String type,String message) {
        // email通信
        if ("email" == type) {
            return email(message);
        }

        // wechat通信
        if ("wechat" == type) {
            return wechat(message);
        }

        return "";
    }

    private String wechat(String message) {
        return "wechat"+message;
    }

    private String email(String message) {
        return "email"+message;
    }

}
