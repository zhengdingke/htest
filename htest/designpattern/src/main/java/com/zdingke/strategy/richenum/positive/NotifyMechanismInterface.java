package com.zdingke.strategy.richenum.positive;

/**
 * @Author Administrator
 * @Date : 2020-12-08 14:05
 * @Description:
 */
public interface NotifyMechanismInterface {

    public static NotifyMechanismInterface byEmail() {
        return new NotifyMechanismInterface() {
            @Override
            public String doNotify(String msg) {
                return "email"+msg;
            }
        };
    }

    public static NotifyMechanismInterface byWechat() {
        return new NotifyMechanismInterface() {
            @Override
            public String doNotify(String msg) {
                return "wechat"+msg;
            }
        };
    }

    public String doNotify(String msg);
    
    
}
