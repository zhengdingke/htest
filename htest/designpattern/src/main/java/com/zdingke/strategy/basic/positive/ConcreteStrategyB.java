package com.zdingke.strategy.basic.positive;

/**
 * @Author Administrator
 * @Date : 2020-12-08 14:51
 * @Description:
 */
public class ConcreteStrategyB implements Strategy {
    @Override
    public void algorithmInterface() {
        System.out.println("实现具体算法B");
    }
}
