package com.zdingke.simplefactory.positive;

import com.zdingke.strategy.basic.positive.Strategy;

/**
 * @Author Administrator
 * @Date : 2020-12-15 17:23
 * @Description:简单工程模式
 */
public abstract class StrategyFactory<T> {
    abstract Strategy createStrategy(Class<T> claz);
}
