package com.zdingke.simplefactory.positive;

import com.zdingke.strategy.basic.positive.Strategy;

/**
 * @Author Administrator
 * @Date : 2020-12-15 17:31
 * @Description:通信策略简单工厂模式
 */
public class NotifyStrategyFactory extends StrategyFactory {
    @Override
    Strategy createStrategy(Class claz) {
        Strategy strategy = null;
        try {
            strategy = (Strategy) Class.forName(claz.getName()).newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return strategy;
    }
}
