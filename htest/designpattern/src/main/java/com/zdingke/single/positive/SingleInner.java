package com.zdingke.single.positive;

/**
 * @Author zhengdingek
 * @Date : 2020-12-16 16:44
 * @Description:静态内部类实现单例模式
 */
public class SingleInner {

    private static class  SingleHolder {
        public static SingleInner instances = new SingleInner();
    }

    private SingleInner(){

    }

    public SingleInner getSingleInstance(){
        return SingleHolder.instances;
    }
}
