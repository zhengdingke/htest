package com.zdingke.single.positive;

/**
 * @Author zhengdingke
 * @Date : 2020-12-16 17:47
 * @Description:enum 方式实现单例模式
 */
public enum SingleEnum {
    INSTANCE;
    public void test(){
        System.out.println("single_test");
    }
}
