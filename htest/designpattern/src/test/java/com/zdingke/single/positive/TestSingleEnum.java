package com.zdingke.single.positive;

import org.junit.Test;

/**
 * @Author zhengdingke
 * @Date : 2020-12-16 17:48
 * @Description:
 */
public class TestSingleEnum {

    @Test
    public void testSingleEnum(){
        SingleEnum.INSTANCE.test();
    }
}
