package com.zdingke.simplefactory.positive;

import com.zdingke.strategy.basic.positive.ConcreteStrategyA;
import com.zdingke.strategy.basic.positive.Strategy;
import org.junit.Test;

/**
 * @Author Administrator
 * @Date : 2020-12-15 18:08
 * @Description:
 */
public class TestSimpleFactory {

    @Test
    public void testSimpleFactory(){
        StrategyFactory factory = new NotifyStrategyFactory();
        Strategy strategy = factory.createStrategy(ConcreteStrategyA.class);
        strategy.algorithmInterface();
    }
}
