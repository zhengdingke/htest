package com.zdingke.strategy.richenum.positive;

import com.zdingke.strategy.richenum.positive.NotifyTypeWithInterface;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @Author Administrator
 * @Date : 2020-12-08 14:14
 * @Description测试使用RichEnum+策略模式去除if-else
 */
public class TestNotifyType {

    private Logger logger = LoggerFactory.getLogger(TestNotifyType.class);

    @Test
    public void testEmailWithInterface(){
        NotifyTypeWithInterface.valueOf("email").getNotifyMechanism().doNotify("dingke");
    }


    @Test
    public void testWechatWithInterface(){
        NotifyTypeWithInterface.valueOf("wechat").getNotifyMechanism().doNotify("dingke");
    }


    @Test
    public void  testEmailWithFunction(){
        String resMsg = NotifyTypeWithFunction.valueOf("email").getNotifyService().apply("dingke");
        logger.info("测试结果：email通信返回结果{}",resMsg);

    }

    @Test
    public void testWechatWithFunction(){
        String resMsg = NotifyTypeWithFunction.valueOf("wechat").getNotifyService().apply("dingke");
        logger.info("测试结果：wechat通信返回结果{}",resMsg);

    }

}
