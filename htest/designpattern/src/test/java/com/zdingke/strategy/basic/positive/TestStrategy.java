package com.zdingke.strategy.basic.positive;

import com.zdingke.strategy.basic.positive.ConcreteStrategyA;
import com.zdingke.strategy.basic.positive.ConcreteStrategyB;
import com.zdingke.strategy.basic.positive.Context;
import org.junit.Test;

/**
 * @Author zhengdingke
 * @Date : 2020-12-08 14:54
 * @Description:测试策略模式
 */
public class TestStrategy {

    @Test
    public void testStrategy(){
        Context contextA = new Context(new ConcreteStrategyA());
        contextA.contextInterface();

        Context contextB = new Context(new ConcreteStrategyB());
        contextB.contextInterface();
    }
}
