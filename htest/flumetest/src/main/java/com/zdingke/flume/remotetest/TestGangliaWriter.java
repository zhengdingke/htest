package com.zdingke.flume.remotetest;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Maps;
import com.zdingke.flume.ganglia.metric.GangliaWriter;

public class TestGangliaWriter {
    private static final Logger log = LoggerFactory.getLogger(TestGangliaWriter.class);

    public static void main(String[] args) {
        if (args[0].equals("single")) {
            sigleThreadWrite(args);
        } else {
            multiThreadWrite(args);
        }
    }

    public static void sigleThreadWrite(String[] args) {
        String host = args[1];
        Integer port = Integer.parseInt(args[2]);
        GangliaWriter writer = GangliaWriter.builder().setHost(host).setPort(port).setAddressingMode(null).setDmax(null).setTmax(null).setUnits("km/h")
                .setSlope(null).setTtl(null).setV31(null).setGroupName("test").build();
        log.info("++++JSON:" + writer.toString());
        Map<String, String> map = Maps.newHashMap();
        map.put("a", "1116");
        map.put("b", "2226");
        writer.doWrite(map);
    }

    public static void multiThreadWrite(String[] args) {

    }


}
