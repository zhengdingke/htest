package com.zdingke.flume.ganglia.metric;

import info.ganglia.gmetric4j.gmetric.GMetric;
import info.ganglia.gmetric4j.gmetric.GMetric.UDPAddressingMode;
import info.ganglia.gmetric4j.gmetric.GMetricSlope;
import info.ganglia.gmetric4j.gmetric.GMetricType;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.Map;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.annotation.JSONCreator;
import com.google.common.base.MoreObjects;

public class GangliaWriter {
    private static final Logger log = LoggerFactory.getLogger(GangliaWriter.class);
    private static final Pattern PATTERN_HOST_IP = Pattern.compile("(.+):([^:]+)$");

    // gmetric命令的配�?
    public static final String ADDRESSING_MODE = "addressingMode";
    public static final String TTL = "ttl";
    public static final String V31 = "v3.1";
    public static final String UNITS = "units";
    public static final String SLOPE = "slope";
    public static final String TMAX = "tmax";
    public static final String DMAX = "dmax";
    public static final String GROUP_NAME = "groupName";
    public static final String SPOOF_NAME = "spoofedHostName";

    // 默认配置
    public static final int DEFAULT_PORT = 8649;
    public static final UDPAddressingMode DEFAULT_ADDRESSING_MODE = UDPAddressingMode.UNICAST;
    public static final int DEFAULT_TTL = 1;
    public static final boolean DEFAULT_V31 = false;
    public static final String DEFAULT_UNITS = "default";
    public static final GMetricSlope DEFAULT_SLOPE = GMetricSlope.BOTH;
    public static final int DEFAULT_DMAX = 6000;
    public static final int DEFAULT_TMAX = 60;
    public static final String DEFAULT_GROUP_NAME = "JMX";

    private String host;
    private Integer port;
    private UDPAddressingMode addressingMode;
    private Integer ttl;
    private Boolean v31;
    private String units;
    private GMetricSlope slope;
    private Integer tmax;
    private Integer dmax;
    private String groupName;

    private String spoofedHostName = null;

    @JSONCreator
    public GangliaWriter(String host, Integer port, String addressingMode, Integer ttl, Boolean v31, String units, String slope, Integer tmax, Integer dmax,
            String groupName) {
        this.host = MoreObjects.firstNonNull(host, null);
        this.port = MoreObjects.firstNonNull(port, DEFAULT_PORT);
        this.addressingMode = computeAddressingMode(MoreObjects.firstNonNull(addressingMode, ""), this.host);
        this.ttl = MoreObjects.firstNonNull(ttl, DEFAULT_TTL);
        this.v31 = MoreObjects.firstNonNull(v31, DEFAULT_V31);
        this.units = MoreObjects.firstNonNull(units, DEFAULT_UNITS);
        this.slope = GMetricSlope.valueOf(MoreObjects.firstNonNull(slope, DEFAULT_SLOPE.name()));
        this.tmax = MoreObjects.firstNonNull(tmax, DEFAULT_TMAX);
        this.dmax = MoreObjects.firstNonNull(dmax, DEFAULT_DMAX);
        this.groupName = MoreObjects.firstNonNull(groupName, DEFAULT_GROUP_NAME);
    }

    private UDPAddressingMode computeAddressingMode(String mode, String host) {
        // Parse and validate addressing mode setting
        try {
            return UDPAddressingMode.valueOf(mode);
        } catch (IllegalArgumentException iae) {
            log.debug("Non existing UDP addressing mode {}.", host, iae);
            try {
                return UDPAddressingMode.getModeForAddress(host);
            } catch (UnknownHostException uhe) {
                log.debug("Unknown host {}, falling back to default addressing mode.", host, uhe);
                return DEFAULT_ADDRESSING_MODE;
            } catch (IOException ioe) {
                log.debug("Could not resolve host {}, falling back to default addressing mode.", host, ioe);
                return DEFAULT_ADDRESSING_MODE;
            }
        }
    }

    private static GMetricType getType(final Object obj) {

        if (obj instanceof Long || obj instanceof Integer || obj instanceof Byte || obj instanceof Short) {
            return GMetricType.INT32;
        }
        if (obj instanceof Float) {
            return GMetricType.FLOAT;
        }
        if (obj instanceof Double) {
            return GMetricType.DOUBLE;
        }

        try {
            Double.parseDouble(obj.toString());
            return GMetricType.DOUBLE;
        } catch (NumberFormatException e) {
        }
        try {
            Integer.parseInt(obj.toString());
            return GMetricType.UINT32;
        } catch (NumberFormatException e) {
        }

        return GMetricType.STRING;
    }

    public void doWrite(Map<String, String> map) {
        map.entrySet().stream().forEach(entry -> {
            String metricName = entry.getKey();
            String metricValue = entry.getValue();
            GMetricType dataType = getType(metricValue);
            log.info("Sending Ganglia metric {}={} [type={}]", entry.getKey(), entry.getValue(), dataType);
            GMetric metric;
            try {
                metric = new GMetric(host, port, addressingMode, ttl);
                metric.announce(metricName, metricValue.toString(), dataType, units, slope, tmax, dmax, groupName);
            } catch (Exception e) {
                log.error("metric announce error,the host {}:{}={}", host, metricName, metricValue, e);
            }
        });
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public UDPAddressingMode getAddressingMode() {
        return addressingMode;
    }

    public void setAddressingMode(UDPAddressingMode addressingMode) {
        this.addressingMode = addressingMode;
    }

    public Integer getTtl() {
        return ttl;
    }

    public void setTtl(Integer ttl) {
        this.ttl = ttl;
    }

    public Boolean getV31() {
        return v31;
    }

    public void setV31(Boolean v31) {
        this.v31 = v31;
    }

    public String getUnits() {
        return units;
    }

    public void setUnits(String units) {
        this.units = units;
    }

    public GMetricSlope getSlope() {
        return slope;
    }

    public void setSlope(GMetricSlope slope) {
        this.slope = slope;
    }

    public Integer getTmax() {
        return tmax;
    }

    public void setTmax(Integer tmax) {
        this.tmax = tmax;
    }

    public Integer getDmax() {
        return dmax;
    }

    public void setDmax(Integer dmax) {
        this.dmax = dmax;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getSpoofedHostName() {
        return spoofedHostName;
    }

    public void setSpoofedHostName(String spoofedHostName) {
        this.spoofedHostName = spoofedHostName;
    }

    @Override
    public String toString() {
        return JSON.toJSONString(this, true);
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {
        private String host;
        private Integer port;
        private String addressingMode;
        private Integer ttl;
        private Boolean v31;
        private String units;
        private String slope;
        private Integer tmax;
        private Integer dmax;
        private String groupName;

        private Builder() {
        }

        public Builder setHost(String host) {
            this.host = host;
            return this;
        }

        public Builder setPort(int port) {
            this.port = port;
            return this;
        }

        public Builder setAddressingMode(String addressingMode) {
            this.addressingMode = addressingMode;
            return this;
        }

        public Builder setTtl(Integer ttl) {
            this.ttl = ttl;
            return this;
        }

        public Builder setV31(Boolean v31) {
            this.v31 = v31;
            return this;
        }

        public Builder setUnits(String units) {
            this.units = units;
            return this;
        }

        public Builder setSlope(String slope) {
            this.slope = slope;
            return this;
        }

        public Builder setTmax(Integer tmax) {
            this.tmax = tmax;
            return this;
        }

        public Builder setDmax(Integer dmax) {
            this.dmax = dmax;
            return this;
        }

        public Builder setGroupName(String groupName) {
            this.groupName = groupName;
            return this;
        }

        public GangliaWriter build() {
            return new GangliaWriter(host, port, addressingMode, ttl, v31, units, slope, tmax, dmax, groupName);
        }

    }

}
