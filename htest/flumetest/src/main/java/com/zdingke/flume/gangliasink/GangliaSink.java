package com.zdingke.flume.gangliasink;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.flume.Channel;
import org.apache.flume.Context;
import org.apache.flume.Event;
import org.apache.flume.EventDeliveryException;
import org.apache.flume.Transaction;
import org.apache.flume.conf.Configurable;
import org.apache.flume.sink.AbstractSink;

import com.alibaba.fastjson.JSON;
import com.zdingke.flume.ganglia.metric.GangliaWriter;


public class GangliaSink extends AbstractSink implements Configurable {

    private static final Log log = LogFactory.getLog(GangliaSink.class);

    @Override
    public Status process() throws EventDeliveryException {
        Channel ch = getChannel();
        Transaction txn = ch.getTransaction();
        try {
            txn.begin();
            Event e = getChannel().take();
            if (e == null) {
                txn.rollback();
                return Status.BACKOFF;
            }
            String data = new String(e.getBody());
            sendDataToGanglia(data);
            txn.commit();
            return Status.READY;
        } catch (Exception ex) {
            txn.rollback();
            log.error(ex.getMessage(), ex);
            return Status.BACKOFF;
        } finally {
            txn.close();
        }
    }

    private void sendDataToGanglia(String data) {
        MetricInfo info = JSON.parseObject(data,MetricInfo.class);
        String []strs= info.getGmetadInfo().split(":");
        GangliaWriter writer = GangliaWriter.builder().setHost(strs[0]).setPort(Integer.parseInt(strs[1])).setAddressingMode(null).setDmax(null).setTmax(null)
                .setUnits("km/h").setSlope(null).setTtl(null).setV31(null).setGroupName(info.getServiceName()).build();
        writer.doWrite(info.getMap());
    }

    @Override
    public void configure(Context context) {
        // TODO Auto-generated method stub

    }
}
