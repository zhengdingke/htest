package com.zdingke.flume.gangliasink;

import java.util.Map;

import com.google.common.collect.Maps;

public class MetricInfo {

    private String serviceName;
    private String gmetadInfo;

    private Map<String, String> map = Maps.newHashMap();

    public MetricInfo() {
    }

    public MetricInfo(String serviceName, Map<String, String> map, String gmetadInfo) {
        this.gmetadInfo = gmetadInfo;
        this.serviceName = serviceName;
        this.map = map;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public Map<String, String> getMap() {
        return map;
    }

    public void setMap(Map<String, String> map) {
        this.map = map;
    }

    public String getGmetadInfo() {
        return gmetadInfo;
    }

    public void setGmetadInfo(String gmetadInfo) {
        this.gmetadInfo = gmetadInfo;
    }

}
