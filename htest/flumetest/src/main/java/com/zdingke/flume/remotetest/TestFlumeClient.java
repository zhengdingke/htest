package com.zdingke.flume.remotetest;

import java.util.Arrays;
import java.util.List;

import com.zdingke.flume.client.GangliaFlumeClient;

public class TestFlumeClient {

    public static void main(String[] args) {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        GangliaFlumeClient client = new GangliaFlumeClient();
//        client.init(host, port);

        List<String> list = Arrays.asList(new String[] { "dingke", "ddddd", "dddd", "aaaa", "bbbb" });
        client.sendListToFlume(list);
    }
}
