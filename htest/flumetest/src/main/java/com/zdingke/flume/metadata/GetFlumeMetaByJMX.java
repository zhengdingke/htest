package com.zdingke.flume.metadata;

import java.io.IOException;
import java.util.Map;

import com.zdingke.common.utils.JMXPollUtil;

public class GetFlumeMetaByJMX {

    public static void main(String[] args) throws IOException {
        Map<String, Map<String, String>> metricmap = JMXPollUtil.getAllMBeans("119.29.28.72", "5445", "org.apache.flume");
        metricmap.entrySet().forEach(m->{
            System.out.println("组件名：" + m.getKey());
        });
    }
}
