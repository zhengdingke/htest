package com.zdingke.flume.client.test;

import org.junit.Test;

import com.zdingke.flume.client.GangliaFlumeClient;

public class TestFlumeClient {
    
    @Test
    public void testGanglia() {
        GangliaFlumeClient client = new GangliaFlumeClient();
        client.init("host.example.org", 41414);

        String sampleData = "HelloFlume!";
        for (int i = 0; i < 10; i++) {
            client.sendDataToFlume(sampleData);
        }
    }
}
