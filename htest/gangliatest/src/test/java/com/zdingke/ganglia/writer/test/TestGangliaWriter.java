package com.zdingke.ganglia.writer.test;

import org.junit.Test;

import com.zdingke.ganglia.writer.GangliaWriter;

public class TestGangliaWriter {

    @Test(expected = NullPointerException.class)
    public void testValidationWithoutSettings() {
        GangliaWriter.builder().build();
    }

    @Test
    public void testValidationAllSettings() {
        GangliaWriter.builder().setHost("192.168.1.144").setPort(25654).setAddressingMode("MULTICAST").setTtl(4).setV31(false).setUnits("km/h")
                .setSlope("NEGATIVE").setTmax(354).setDmax(24).setGroupName("dummy").build();
    }
}
